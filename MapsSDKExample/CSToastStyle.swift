//
//  CSToastStyle.swift
//  MapsSDKExample
//
//  Created by 洪德晟 on 08/12/2017.
//  Copyright © 2017 洪德晟. All rights reserved.
//

import UIKit

var CSToastPositionTop = "CSToastPositionTop"
var CSToastPositionCenter = "CSToastPositionCenter"
var CSToastPositionBottom = "CSToastPositionBottom"

// Keys for values associated with toast views
private let CSToastTimerKey = "CSToastTimerKey"
private let CSToastDurationKey = "CSToastDurationKey"
private let CSToastPositionKey = "CSToastPositionKey"
private let CSToastCompletionKey = "CSToastCompletionKey"

// Keys for values associated with self
private let CSToastActiveToastViewKey = "CSToastActiveToastViewKey"
private let CSToastActivityViewKey = "CSToastActivityViewKey"
private let CSToastQueueKey = "CSToastQueueKey"

extension UIView {
    /**
     These private methods are being prefixed with "cs_" to reduce the likelihood of non-obvious
     naming conflicts with other UIView methods.
     
     @discussion Should the public API also use the cs_ prefix? Technically it should, but it
     results in code that is less legible. The current public method names seem unlikely to cause
     conflicts so I think we should favor the cleaner API for now.
     */
    
    // MARK: - Private Show/Hide Methods
    func cs_showToast(_ toast: UIView, duration: TimeInterval, position: Any) {
        
//        toast.center = cs_centerPoint(forPosition: position, withToast: toast)
//        toast.alpha = 0.0
//        if CSToastManager.isTapToDismissEnabled() {
//            let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.cs_handleToastTapped))
//            toast.addGestureRecognizer(recognizer)
//            toast.isUserInteractionEnabled = true
//            toast.isExclusiveTouch = true
//        }
//
//        // set the active toast
//        objc_setAssociatedObject(self, CSToastActiveToastViewKey, toast, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//        addSubview(toast)
//        UIView.animate(withDuration: CSToastManager.sharedStyle().fadeDuration as? TimeInterval ?? 0.0, delay: 0.0, options: [.curveEaseOut, .allowUserInteraction], animations: {() -> Void in
//            toast.alpha = 1.0
//        }, completion: {(_ finished: Bool) -> Void in
//            let timer = Timer(timeInterval: duration, target: self, selector: #selector(self.cs_toastTimerDidFinish), userInfo: toast, repeats: false)
//            RunLoop.main.add(timer, forMode: .commonModes)
//            objc_setAssociatedObject(toast, CSToastTimerKey, timer, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//        })
    }
    
    func cs_hideToast(_ toast: UIView) {
        cs_hideToast(toast, fromTap: false)
    }
    
    func cs_hideToast(_ toast: UIView, fromTap: Bool) {
//        UIView.animate(withDuration: CSToastManager.sharedStyle().fadeDuration() as? TimeInterval ?? 0.0, delay: 0.0, options: [.curveEaseIn, .beginFromCurrentState], animations: {() -> Void in
//            toast.alpha = 0.0
//        }, completion: {(_ finished: Bool) -> Void in
//            toast.removeFromSuperview()
//            // clear the active toast
//            objc_setAssociatedObject(self, CSToastActiveToastViewKey, nil, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            // execute the completion block, if necessary
//            let completion: ((_ didTap: Bool) -> Void)? = objc_getAssociatedObject(toast, CSToastCompletionKey)
//            if completion != nil {
//                completion(fromTap)
//            }
//            if self.cs_toastQueue.count() > 0 {
//                // dequeue
//                var nextToast: UIView? = cs_toastQueue().first
//                cs_toastQueue().remove(at: 0)
//                // present the next toast
//                var duration = objc_getAssociatedObject(nextToast, CSToastDurationKey) as? TimeInterval
//                var position = objc_getAssociatedObject(nextToast, CSToastPositionKey)
//                cs_showToast(nextToast, duration: duration, position: position)
//            })
    }
    
    func cs_toastTimerDidFinish(_ timer: Timer) {
    }
    
    @objc func cs_handleToastTapped(_ recognizer: UITapGestureRecognizer) {
    }
    
//    func cs_centerPoint(forPosition position: Any, withToast toast: UIView) -> CGPoint {
//    }
    
//    func cs_toastQueue() -> [AnyHashable] {
//    }
}

extension UIView {
    
    // MARK: - Make Toast Methods
    func makeToast(_ message: String) {
//        makeToast(message, duration: CSToastManager.defaultDuration(), position: CSToastManager.defaultPosition(), style: nil)
    }
    
    func makeToast(_ message: String, duration: TimeInterval, position: Any) {
//        makeToast(message, duration: duration, position: position, style: nil)
    }
    
    func makeToast(_ message: String, duration: TimeInterval, position: Any, style: CSToastStyle) {
//        let toast: UIView? = toastView(forMessage: message, title: nil, image: nil, style: style)
//        showToast(toast, duration: duration, position: position) { _ in }
    }
    
    func makeToast(_ message: String, duration: TimeInterval, position: Any, title: String, image: UIImage, style: CSToastStyle, completion: @escaping (_ didTap: Bool) -> Void) {
//        let toast: UIView? = toastView(forMessage: message, title: title, image: image, style: style)
//        showToast(toast, duration: duration, position: position, completion: completion)
    }
    
//    func toastView(forMessage message: String, title: String, image: UIImage, style: CSToastStyle) -> UIView {
//    }
    
    func makeToastActivity(_ position: Any) {
    }
    
    func hideToastActivity() {
    }
    
    // MARK: - Show Toast Methods
//    func showToast(_ toast: UIView) {
//        showToast(toast, duration: CSToastManager.defaultDuration(), position: CSToastManager.defaultPosition()) { _ in }
//    }
    
    func showToast(_ toast: UIView, duration: TimeInterval, position: Any, completion: @escaping (_ didTap: Bool) -> Void) {
//        // sanity
//        if toast == nil {
//            return
//        }
//        // store the completion block on the toast view
//        objc_setAssociatedObject(toast, CSToastCompletionKey, completion, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//        if CSToastManager.isQueueEnabled() && objc_getAssociatedObject(self, CSToastActiveToastViewKey) != nil {
//            // we're about to queue this toast view so we need to store the duration and position as well
//            objc_setAssociatedObject(toast, CSToastDurationKey, duration, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            objc_setAssociatedObject(toast, CSToastPositionKey, position, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            // enqueue
//            cs_toastQueue().append(toast)
//        }
//        else {
//            // present
//            cs_showToast(toast, duration: duration, position: position)
//        }
    }
    
    func hideToast(_ toast: UIView) {
        cs_hideToast(toast, fromTap: false)
    }
}

class CSToastStyle: NSObject {
    
    /**
     The background color. Default is `[UIColor blackColor]` at 80% opacity.
     */
    var backgroundColor: UIColor?
    
    /**
     The title color. Default is `[UIColor whiteColor]`.
     */
    var titleColor: UIColor?
    
    /**
     The message color. Default is `[UIColor whiteColor]`.
     */
    var messageColor: UIColor?
    
    /**
     A percentage value from 0.0 to 1.0, representing the maximum width of the toast
     view relative to it's superview. Default is 0.8 (80% of the superview's width).
     */
    var maxWidthPercentage: CGFloat = 0.0
    
    /**
     A percentage value from 0.0 to 1.0, representing the maximum height of the toast
     view relative to it's superview. Default is 0.8 (80% of the superview's height).
     */
    var maxHeightPercentage: CGFloat = 0.0
    
    /**
     The spacing from the horizontal edge of the toast view to the content. When an image
     is present, this is also used as the padding between the image and the text.
     Default is 10.0.
     */
    var horizontalPadding: CGFloat = 0.0
    
    /**
     The spacing from the vertical edge of the toast view to the content. When a title
     is present, this is also used as the padding between the title and the message.
     Default is 10.0.
     */
    var verticalPadding: CGFloat = 0.0
    
    /**
     The corner radius. Default is 10.0.
     */
    var cornerRadius: CGFloat = 0.0
    
    /**
     The title font. Default is `[UIFont boldSystemFontOfSize:16.0]`.
     */
    var titleFont: UIFont?
    
    /**
     The message font. Default is `[UIFont systemFontOfSize:16.0]`.
     */
    var messageFont: UIFont?
    
    /**
     The title text alignment. Default is `NSTextAlignmentLeft`.
     */
    var titleAlignment: NSTextAlignment?
    
    /**
     The message text alignment. Default is `NSTextAlignmentLeft`.
     */
    
    var messageAlignment: NSTextAlignment?
    
    /**
     The maximum number of lines for the title. The default is 0 (no limit).
     */
    var titleNumberOfLines: Int = 0
    
    /**
     The maximum number of lines for the message. The default is 0 (no limit).
     */
    var messageNumberOfLines: Int = 0
    
    /**
     Enable or disable a shadow on the toast view. Default is `NO`.
     */
    var isDisplayShadow = false
    
    /**
     The shadow color. Default is `[UIColor blackColor]`.
     */
    var shadowColor: UIColor?
    
    /**
     A value from 0.0 to 1.0, representing the opacity of the shadow.
     Default is 0.8 (80% opacity).
     */
    var shadowOpacity: CGFloat = 0.0
    
    /**
     The shadow radius. Default is 6.0.
     */
    var shadowRadius: CGFloat = 0.0
    
    /**
     The shadow offset. The default is `CGSizeMake(4.0, 4.0)`.
     */
    var shadowOffset = CGSize.zero
    
    /**
     The image size. The default is `CGSizeMake(80.0, 80.0)`.
     */
    var imageSize = CGSize.zero
    
    /**
     The size of the toast activity view when `makeToastActivity:` is called.
     Default is `CGSizeMake(100.0, 100.0)`.
     */
    var activitySize = CGSize.zero
    
    /**
     The fade in/out animation duration. Default is 0.2.
     */
    var fadeDuration: TimeInterval = 0.0
    
    /**
     Creates a new instance of `CSToastStyle` with all the default values set.
     */
//    var NS_DESIGNATED_INITIALIZER: initWithDefaultStyle?
    
    /**
     @warning Only the designated initializer should be used to create
     an instance of `CSToastStyle`.
     */
//    var NS_UNAVAILABLE: `init`?
}

class CSToastManager: NSObject {
    /**
     Sets the shared style on the singleton. The shared style is used whenever
     a `makeToast:` method (or `toastViewForMessage:title:image:style:`) is called
     with with a nil style. By default, this is set to `CSToastStyle`'s default
     style.
     
     @param sharedStyle
     */
    
    class func setSharedStyle(_ sharedStyle: CSToastStyle) {
    }
    
    /**
     Gets the shared style from the singlton. By default, this is
     `CSToastStyle`'s default style.
     
     @return the shared style
     */
//    class func sharedStyle() -> CSToastStyle {
//    }
    
    /**
     Enables or disables tap to dismiss on toast views. Default is `YES`.
     
     @param allowTapToDismiss
     */
    class func setTapToDismissEnabled(_ tapToDismissEnabled: Bool) {
    }
    
    /**
     Returns `YES` if tap to dismiss is enabled, otherwise `NO`.
     Default is `YES`.
     
     @return BOOL
     */
    
//    class func isTapToDismissEnabled() -> Bool {
//    }
    
    /**
     Enables or disables queueing behavior for toast views. When `YES`,
     toast views will appear one after the other. When `NO`, multiple Toast
     views will appear at the same time (potentially overlapping depending
     on their positions). This has no effect on the toast activity view,
     which operates independently of normal toast views. Default is `YES`.
     
     @param queueEnabled
     */
    
    class func setQueueEnabled(_ queueEnabled: Bool) {
    }
    
    /**
     Returns `YES` if the queue is enabled, otherwise `NO`.
     Default is `YES`.
     
     @return BOOL
     */
    
//    class func isQueueEnabled() -> Bool {
//    }

    /**
     Sets the default duration. Used for the `makeToast:` and
     `showToast:` methods that don't require an explicit duration.
     Default is 3.0.
     
     @param duration The toast duration
     */
    
    class func setDefaultDuration(_ duration: TimeInterval) {
    }
    
    /**
     Returns the default duration. Default is 3.0.
     
     @return duration The toast duration
     */
    
//    class func defaultDuration() -> TimeInterval {
//    }
    
    /**
     Sets the default position. Used for the `makeToast:` and
     `showToast:` methods that don't require an explicit position.
     Default is `CSToastPositionBottom`.
     
     @param position The default center point. Can be one of the predefined
     CSToastPosition constants or a `CGPoint` wrapped in an `NSValue` object.
     */
    
    class func setDefaultPosition(_ position: Any) {
    }
    
    /**
     Returns the default toast position. Default is `CSToastPositionBottom`.
     
     @return position The default center point. Will be one of the predefined
     CSToastPosition constants or a `CGPoint` wrapped in an `NSValue` object.
     */
    
//    class func defaultPosition() -> Any {
//    }

}
