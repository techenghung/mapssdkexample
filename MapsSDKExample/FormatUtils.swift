//
//  FormatUtils.swift
//  MapsSDKExample
//
//  Created by 洪德晟 on 08/12/2017.
//  Copyright © 2017 洪德晟. All rights reserved.
//

import Foundation

class TimeRemainingPieces: NSObject {
    public var value = ""
    public var unit = ""
    public var subValue = ""
    public var subUnit = ""
    public var combinedString = ""

    init(seconds: Int) {
        super.init()
        
        if seconds >= 86400 {
            let days: Int = seconds / 86400
            let hours: Int = (seconds % 86400) / 3600
            value = "\(days)"
            unit = "d"
            subValue = "\(hours)"
            subUnit = "h"
        }
        else if seconds >= 3600 {
            let hours: Int = seconds / 3600
            let minutes: Int = (seconds % 3600) / 60
            value = "\(hours)"
            unit = "h"
            subValue = "\(minutes)"
            subUnit = "m"
        }
        else {
            let minutes: Int = seconds / 60
            value = "\(minutes > 0 ? minutes : 1)"
            unit = " min"
        }
        
        if (subValue != "") {
            combinedString = "\(value)\(unit) \(subValue)\(subUnit)"
        }
        else {
            combinedString = "\(value)\(unit)"
        }
    }
}

class ArrivalTimePieces: NSObject {
    public var timeString = ""
    public var dataRange = NSRange()

    init(seconds: Int) {
        super.init()
        
        let arrivalTime = Date(timeIntervalSinceNow: seconds as? TimeInterval ?? 0.0)
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        timeString = formatter.string(from: arrivalTime)
//        dataRange = timeString.range(of: formatter.amSymbol) as NSRange
//        if dataRange.location == NSNotFound {
//            dataRange = timeString.range(of: formatter.pmSymbol)
//        }
//        if dataRange.location != NSNotFound {
//            dataRange = Range(location: 0, length: dataRange.location)
//        }
        
    }
}

class DistancePieces: NSObject {
    public var value = ""
    public var unit = ""
    public var combined = ""
    
    init(meters: Int, imperial: Bool) {
        super.init()
        
        var fValue: Float = 0
        var iValue: Int = 0
        if imperial {
            let feet = Float((Double(meters) * 3.28084))
            if feet <= 500 {
                iValue = Int(ceilf(feet))
                unit = "ft"
            }
            else {
                let miles: Float = feet / 5280.0
                fValue = miles
                unit = "mi"
            }
        }
        else {
            if meters <= 500 {
                iValue = (meters > 0) ? meters : 1
                unit = "m"
            }
            else {
                let km = Float((Double(meters) / 1000.0))
                fValue = km
                unit = "km"
            }
        }
        if fValue != 0.0 {
            var formatter: NumberFormatter? = nil
            if formatter == nil {
                formatter = NumberFormatter()
                formatter?.maximumFractionDigits = 1
                formatter?.minimumIntegerDigits = 1
                formatter?.usesGroupingSeparator = true
                formatter?.groupingSize = 3
            }
            value = (formatter?.string(from: fValue as NSNumber))!
        } else {
            value = "\(iValue)"
        }
        combined = "\(value) \(unit)"
    }
}

class FormatUtils: NSObject {
    class func formatTimeRemaining(_ seconds: Int) -> String {
        let pieces = TimeRemainingPieces(seconds: seconds)
        return pieces.combinedString
    }
    
    class func formatArrivalTime(_ seconds: Int) -> String {
        let pieces = ArrivalTimePieces(seconds: seconds)
        return pieces.timeString
    }
    
    class func formatDistance(_ meters: Int, imperial: Bool) -> String {
        let pieces = DistancePieces(meters: meters, imperial: imperial)
        return pieces.combined
    }
    
    class func formatTimeRemaining(_ label: UILabel, seconds: Int) -> NSAttributedString {
        var result: NSMutableAttributedString? = nil
        let boldFont: UIFont? = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        let pieces = TimeRemainingPieces(seconds: seconds)
        result = NSMutableAttributedString(string: pieces.combinedString)
        result?.addAttribute(NSAttributedStringKey.font, value: boldFont ?? 0, range: NSRange(location: 0, length: pieces.value.count))
        if pieces.subValue != "" {
            result?.addAttribute(NSAttributedStringKey.font, value: boldFont ?? 0, range: NSRange(location: pieces.value.count + 2, length: pieces.subValue.count))
        }
        return result ?? NSAttributedString()
    }
    
    class func formatArrivalTime(_ label: UILabel, seconds: Int) -> NSAttributedString {
        var result: NSMutableAttributedString?
        let pieces = ArrivalTimePieces(seconds: seconds)
        result = NSMutableAttributedString(string: pieces.timeString)
        result?.addAttribute(NSAttributedStringKey.font, value: UIFont.boldSystemFont(ofSize: label.font.pointSize), range: pieces.dataRange)
        return result ?? NSAttributedString()
    }
    
    class func formatDistance(_ label: UILabel, meters: Int, imperial: Bool) -> NSAttributedString {
        var result: NSMutableAttributedString? = nil
        let pieces = DistancePieces(meters: meters, imperial: imperial)
        result = NSMutableAttributedString(string: pieces.combined)
        result?.addAttribute(NSAttributedStringKey.font, value: UIFont.boldSystemFont(ofSize: label.font.pointSize), range: NSRange(location: 0, length: pieces.value.count))
        return result ?? NSAttributedString()
    }
    
    class func formatText(_ label: UILabel, text: String) -> NSAttributedString {
        var result: NSMutableAttributedString? = nil
        let boldFont: UIFont? = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        result = NSMutableAttributedString(string: text)
        result?.addAttribute(NSAttributedStringKey.font, value: boldFont ?? 0, range: NSRange(location: 0, length: (text.count ?? 0)))
        return result ?? NSAttributedString()
    }

}
