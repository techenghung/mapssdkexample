//
//  NavigationLaneView.swift
//  MapsSDKExample
//
//  Created by 洪德晟 on 07/12/2017.
//  Copyright © 2017 洪德晟. All rights reserved.
//

import UIKit
import QuartzCore

let LANE_COUNT_MAX: Int = 6

class NavigationLaneView: UIView {
    var laneIcons: NSMutableArray?
    
    private func configureView() {
        // Create the image view
        var imageRect: CGRect = self.bounds
        let viewW: Int = Int(imageRect.size.width)
        let perLaneW: Int = viewW / LANE_COUNT_MAX
        
        imageRect.size.width = CGFloat(perLaneW)
        let icons = NSMutableArray.init(capacity: LANE_COUNT_MAX)
        for _ in 0..<LANE_COUNT_MAX {
            let icon: UIImageView = UIImageView.init(frame: imageRect)
            icons.add(icon)
            self.addSubview(icon)
            imageRect.origin.x += CGFloat(perLaneW)
        }
        self.laneIcons = icons
        self.isHidden = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configureView()
        fatalError("init(coder:) has not been implemented")
    }
}
