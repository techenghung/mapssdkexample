//
//  OverlayDrawing.swift
//  MapsSDKExample
//
//  Created by 洪德晟 on 08/12/2017.
//  Copyright © 2017 洪德晟. All rights reserved.
//

import Foundation

private func randomRatio() -> Float {
    return Float(arc4random() & 0xffff) / 0xffff
}

private func deg2rad(degrees: Float) -> Float {
    return degrees * (.pi / 180.0)
}
