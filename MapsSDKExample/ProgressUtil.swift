//
//  ProgressUtil.swift
//  MapsSDKExample
//
//  Created by 洪德晟 on 08/12/2017.
//  Copyright © 2017 洪德晟. All rights reserved.
//

import Foundation

class ProgressUtil: NSObject {
    var progress: UIAlertController?
    var vcontroller: UIViewController?
    var ptitle = ""
    var psubtitle = ""
    
    init(viewController viewcontroller: UIViewController, title: String, subtitle: String) {
        super.init()
        
        vcontroller = viewcontroller
        ptitle = title
        psubtitle = subtitle
        
    }
    
    func displayProgress(withAction callback: @escaping (_: Void) -> Void) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        progress = UIAlertController(title: ptitle, message: psubtitle, preferredStyle: .alert)
        progress?.addAction((UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        })))
        let customVC = UIViewController()
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        customVC.view.addSubview(spinner)
        customVC.view.addConstraint(NSLayoutConstraint(item: spinner, attribute: .centerX, relatedBy: .equal, toItem: customVC.view, attribute: .centerX, multiplier: 1.0, constant: 0.0))
        customVC.view.addConstraint(NSLayoutConstraint(item: spinner, attribute: .centerY, relatedBy: .equal, toItem: customVC.view, attribute: .centerY, multiplier: 1.0, constant: 0.0))
        progress?.setValue(customVC, forKey: "contentViewController")
        if let count = vcontroller?.navigationController?.viewControllers.count, count > 1 {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(1 * Double(NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {() -> Void in
                self.vcontroller?.present(self.progress!, animated: true)
            })
        }
        else {
            vcontroller?.present(self.progress!, animated: true)
        }
        
        
    }
    
    func cancelProgress() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        progress?.dismiss(animated: true)
    }
}
