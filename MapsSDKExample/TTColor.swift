//
//  TTColor.swift
//  MapsSDKExample
//
//  Created by 洪德晟 on 07/12/2017.
//  Copyright © 2017 洪德晟. All rights reserved.
//

import UIKit

let TTColorWhite      = UIColor.white
let TTColorLightStone = UIColor(red: 245.0 / 255.0, green: 245.0 / 255.0, blue: 245.0 / 255.0, alpha: 1)
let TTColorStone      = UIColor(red: 204.0 / 255.0, green: 204.0 / 255.0, blue: 204.0 / 255.0, alpha: 1)
let TTColorLight      = UIColor(red: 178.0 / 255.0, green: 178.0 / 255.0, blue: 178.0 / 255.0, alpha: 1)
let TTColorMidgrey    = UIColor(red: 153.0 / 255.0, green: 153.0 / 255.0, blue: 153.0 / 255.0, alpha: 1)
let TTColorDimgrey    = UIColor(red: 68.0 / 255.0, green: 68.0 / 255.0, blue: 68.0 / 255.0, alpha: 1)
let TTColorDarkGrey   = UIColor(red: 38.0 / 255.0, green: 38.0 / 255.0, blue: 38.0 / 255.0, alpha: 1)
let TTColorBlack      = UIColor.black
let TTColorGreen      = UIColor(red: 189.0 / 255.0, green: 215.0 / 255.0, blue: 49.0 / 255.0, alpha: 1)
let TTColorForest     = UIColor(red: 122.0 / 255.0, green: 169.0 / 255.0, blue: 32.0 / 255.0, alpha: 1)

