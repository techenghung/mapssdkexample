//
//  TTLocation.swift
//  MapsSDKExample
//
//  Created by 洪德晟 on 07/12/2017.
//  Copyright © 2017 洪德晟. All rights reserved.
//

import Foundation

let COORDINATE_AMSTERDAM: CLLocationCoordinate2D = CLLocationCoordinate2DMake(25.002205, 121.51336)
let COORDINATE_ROTTERDAM: CLLocationCoordinate2D = CLLocationCoordinate2DMake(51.935966, 4.482865)
let COORDINATE_MY: CLLocationCoordinate2D = CLLocationCoordinate2DMake(25.046000, 121.515250)
