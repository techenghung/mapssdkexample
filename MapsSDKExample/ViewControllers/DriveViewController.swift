//
//  DriveViewController.swift
//  MapsSDKExample
//
//  Created by 洪德晟 on 07/12/2017.
//  Copyright © 2017 洪德晟. All rights reserved.
//

import UIKit

protocol DriveViewControllerDelegate {
    func centerCurrentLocationHandler()
}

let kManeuverIconWidth: CGFloat = 46

class DriveViewController: UIViewController {
    
    @IBOutlet weak var laneView:                 NavigationLaneView!
    @IBOutlet weak var etaTimeLabel:             UILabel!
    @IBOutlet weak var etaDistanceLabel:         UILabel!
    @IBOutlet weak var directionImageView:       UIImageView!
    @IBOutlet weak var directionDistance:        UILabel!
    @IBOutlet weak var nextDirectionStreetLabel: UILabel!
    @IBOutlet weak var currentStreetLabel:       UILabel!
    @IBOutlet weak var currentSpeedLabel:        UILabel!
    @IBOutlet weak var speedLimitImageView:      UIImageView!
    
    @IBAction func centerCurrentLocation(sender: UIButton){
        
    }
    
    var delegate: DriveViewControllerDelegate?
    static var maneuverIconWidth: CGFloat {
        return kManeuverIconWidth
    }

    override func viewDidLoad() {
        super.viewDidLoad()

    }
 
    func updateDriveView(_ update: TTNavigationUpdate, onMap navViewController: TTNavViewController) {
        currentStreetLabel.text = (update.guidance.currentStreet() != nil) ? update.guidance.currentStreet() : ""
        nextDirectionStreetLabel.text = (update.guidance.nextStreet() != nil) ? update.guidance.nextStreet() : ""
//        directionImageView.image = (navViewController.generateImages(forManeuverIcon: update.guidance.maneuverIcon()).last != nil) ? navViewController.generateImages(forManeuverIcon: update.guidance.maneuverIcon()).last : nil
//        let dir: String = FormatUtils.formatDistance(directionDistance, meters: update.guidance.distanceToCrossing, imperial: false).string()
//        directionDistance.text = dir
//        let speed = "\(Int(update.speedKPH)) Km/h"
//        currentSpeedLabel.text = speed
//        let etaTime: String = FormatUtils.formatTimeRemaining(etaTimeLabel, seconds: update.guidance.secondsToArrival).string()
//        etaTimeLabel.text = etaTime
//        let etaDist: String = FormatUtils.formatDistance(etaDistanceLabel, meters: update.guidance.distanceToDestination, imperial: false).string()
//        etaDistanceLabel.text = etaDist
//        updateSpeedLimit(update)
//        updateLaneGuidance(update.guidance.guidanceLane)
    }
}
