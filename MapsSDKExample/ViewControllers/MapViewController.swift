//
//  ViewController.swift
//  MapsSDKExample
//
//  Created by Dan on 04/12/2017.
//  Copyright © 2017 Dan. All rights reserved.
//

import UIKit
import CoreLocation
import CoreGraphics
import AudioToolbox
import AVFoundation

enum MapViewOptionsTag : Int {
    case none
    case routeTypes
    case routeAvoids
    case travelMode
    case routingWithWaypoints
    case trafficLayer
    case mapLayersLayer
    case mapTilesLayer
    case map2D3D
    case centeringMap
    case annotations
    case balloons
    case shapes
    case speedDependentZoom
}

enum NightModeOption : Int {
    case automatic
    case day
    case night
}

let DEFAULT_SPEED_SIMULATION: Int32 = 3

class MapViewController: UIMapViewController {
    
    var guidanceConfig       : TTGuidanceConfig?
    var configProperties     : String = ""
    var mapProperties        : String = ""
    var loggingProperties    : String = ""
    var mapIcon              : TTMapIcon?
    var lastUpdate           : TTNavigationUpdate?
    var lastMultipeConfigs   : Array<TTGuidanceConfig> = [TTGuidanceConfig]()
    var lastSingleConfig     : TTGuidanceConfig?
    var guidanceActive       : Bool = false
    var singleConfigMasks    : Array<Any> = [Any]()
    var isGuidanceActive     : Bool = false
    var navigationUpdateTemp : TTNavigationUpdate?
    var droppedOverlayItems  : Array<AnyHashable> = [AnyHashable]()
    
    private var navigationUpdateConnectionRetain: Any?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CLLocationManager().startUpdatingLocation()
        delegate = self
        droppedOverlayItems = [AnyHashable]()
        mapView.setCompassPositionHorizontalRatio(0.0, verticalRatio: 0.15)
//        progressUtil = ProgressUtil(viewController: self, title: "Calculating route", subtitle: "Wait for route to be planned")
        initializeNavigationAudio()
        initializeMap()
        initializeNavigation()
        createMenuNavigationItem()
//        if let currentLocation = CLLocationManager().location {
//            centerOnCoordinates(coordinates: currentLocation.coordinate, withZoomLevel: 10)
//        }
    }
    
    // MARK: - Map View Controller
    func loadConfigFile() -> TTConfigFile? {
        let config = TTConfigFile.withDefaultPath()
        return config
    }
    
    
    private func initializeMap() {
        if let file = self.loadConfigFile() {
            let mapLaunchState = TTMapLaunchState.init(configFile: file)
            mapView.launchState = mapLaunchState
        }
    }
    
    func initializeNavigationAudio() {
        var audioError: Error? = nil
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: .mixWithOthers)
        try? AVAudioSession.sharedInstance().setActive(true)
    }

    
    private func initializeNavigation() {
        //Create configuration for navigation/guidance
        let navigationConfig = TTNavigationConfig(configFile: self.loadConfigFile())
        
        navigationConfig?.resourceDir = String(format: "%@/nav_resources", Bundle.main.resourcePath!)
        
        //Create navigation manager and set it as in TTNavViewController
        let navigation = TTNavigationManager(config: navigationConfig)
        self.navigationManager = navigation
        
        //Register navigation delegate
        //Important is to keep reference to to auto released object
        self.navigationManager.registerForNavigationUpdates(with: self)
        
        //Obtain user current location
        if let currentLocation = CLLocationManager().location {
            //Initialize navigation with launch state and set it as in TTNavViewController
            let navLaunchState = TTNavLaunchState(
                vehiclePositon: currentLocation.coordinate,
                direction: currentLocation.course)
            self.launchState = navLaunchState
        }
        
    }
    
    private func moveMapToCurrentLocation() {
    
        if let currentLocation = CLLocationManager().location {
            //center on current location with North up and not chnaged perspective
            self.currentMapOrientation = TTMapOrientation(focus: currentLocation.coordinate, zoomLevel: 20, perspectiveRatio: self.mapView.overheadRatio, yawDegrees: 0) // North Up
        }
        
        self.mapView.setMapOrientation(currentMapOrientation, animated: false)
    
    }
    
    private func clearCurrentRoute() {
        self.guidanceActive = false
        UIApplication.shared.isIdleTimerDisabled = false
        
        self.clearRoute()
        self.clearDestination()
        self.navigationManager.stopGuidance()
        
        self.guidanceConfig?.destination = kCLLocationCoordinate2DInvalid
        
        if let currentLocation = CLLocationManager().location {
            self.updateVehiclePosition(currentLocation.coordinate, direction: currentLocation.course)
        }
    }
    
    private func invalidateMapSettings() {
        // MapConfiguration is just simple example of model how to store map properties
//        MapConfiguration *mapConfig = [MapConfiguration sharedInstance];
        
//        self.mapView.setSatelliteVisible(<#T##visible: Bool##Bool#>, hybrid: <#T##Bool#>, animated: <#T##Bool#>)
        
//        self.mapView.setTrafficVisible(<#T##visible: Bool##Bool#>, animated: <#T##Bool#>)
//        self.mapView.setTrafficIncidentsVisible(<#T##visible: Bool##Bool#>, animated: <#T##Bool#>)
//        self.mapView.overheadRatio = mapConfig.mapViewType
    }
    
    private func shouldStartNavigationWhenRouteReady() -> Bool {
        return self.lastSingleConfig == nil
    }
    
    // MARK: - Create Guidances
    func createGuidance
        (
        origin:           CLLocationCoordinate2D,
        destination dest: CLLocationCoordinate2D,
        routeOption:      TTGuidanceRouteOption,
        simulation:       Bool,
        speed:            Int32
        ) -> TTGuidanceConfig?
    {
        
        let config = TTGuidanceConfig(destination: dest, origin: origin)
        config?.clearAllRouteOptions()
        config?.setRouteOption(routeOption)
        
        config?.simulate = simulation
        config?.simulationSpeed = speed
        
        return config
    }
    
    func createGuidance
        (
        origin:           CLLocationCoordinate2D,
        destination dest: CLLocationCoordinate2D,
        routeMode:        TTGuidanceRouteMode,
        simulation:       Bool,
        speed:            Int32
        ) -> TTGuidanceConfig?
    {
        
        let config = TTGuidanceConfig(destination: dest, origin: origin)
        config?.clearAllRouteOptions()
        config?.routeMode = routeMode
        
        config?.simulate = simulation
        config?.simulationSpeed = speed
        
        return config
    }
    
    // MARK: - TTMapDelegate
    override func doneLoading(_ mapView: TTMapView!) {
        super.doneLoading(mapView)
        moveMapToCurrentLocation()
        
        let primaryColor: UIColor = TTColorGreen
        let secondaryColor: UIColor = TTColorLightStone
        let retinaFactor: Int = Int(UIScreen.main.scale - 1.0)
        let maneuverIconDim: Int = Int(DriveViewController.maneuverIconWidth)
        
        self.configureManeuverIcons(withSize: Int32(maneuverIconDim << retinaFactor), colorImages: true, dualLayered: false, primaryColor: primaryColor, secondaryColor:secondaryColor)
        
        if let currentLocation = CLLocationManager().location {
            self.updateVehiclePosition(currentLocation.coordinate
            , direction: currentLocation.course)
            
            
        }
        self.center(onCoordinates: COORDINATE_MY, withZoomLevel: 14)
        self.mapView.setTileFormatConfiguration(TileFormatConfigurationVectorTiles)
    }
    
    // MARK: - RoutePlannerProtocol
    private func startNavigationWithConfig(_ config: TTGuidanceConfig) {
    
        self.setNavigationCameraActive(false, cancelNorthUp: false)
        self.clearCurrentRoute()
        self.navigationManager.startGuidance(config)
        self.setDestination(config.destination)
        
        if let count = self.navigationController?.viewControllers.count, count > 2 {
            self.navigationController?.popViewController(animated: true)
        }
        progressUtil?.displayProgress(withAction: {() -> Void in
            self.stopNavigation(nil)
            self.clearCurrentRoute()
            self.hideDriveView()
        })
    }
    
    private func prepareRouteWithSignleChoiceConfigsConfig(_ configs: Array<TTGuidanceConfig>) {
        self.lastMultipeConfigs = configs
        self.lastSingleConfig = nil
    }
    
    func planRouteWithSignleChoiceConfigs(_ configs: Array<TTGuidanceConfig>, index: Int) {
        self.prepareRouteWithSignleChoiceConfigsConfig(configs)
        self.startNavigationWithConfig(configs[index])
    }
    
    private func center(onCoordinates: CLLocationCoordinate2D , withZoomLevel zoomLevel: Int32) {
        self.currentMapOrientation = TTMapOrientation(focus: onCoordinates, zoomLevel: zoomLevel, perspectiveRatio: self.mapView.overheadRatio, yawDegrees: 0)
        
        self.mapView.setMapOrientation(currentMapOrientation, animated: false)
        //        self.setSubtitleForCoordinate(coordinates)
    }
    
    func center(onCoordinates: CLLocationCoordinate2D) {
        self.center(onCoordinates: onCoordinates, withZoomLevel: 10)
    }
    
    // MARK: - Maneuer Section
    func maneuverViewControllerIsOnTop() -> Bool {
//        return navigationController?.topViewController is ManeuverViewController
        return false
    }
    
    func performManeuver(_ navigationUpdate: TTNavigationUpdate) {
//        navigationUpdateTemp = navigationUpdate
//        var items = [AnyHashable]() as? [ManeuverItem]
//
//        guard let directionsList = navigationUpdateTemp?.guidance.directionsList() as? [TTGuidanceListItem] else {
//            return
//        }
//
//        for _item: TTGuidanceListItem in directionsList {
//            let item = ManeuverItem(distance: self.item.distance(), description: self.item.description(), image: (generateImages(forManeuverIcon: self.item.icon()).last as? UIImage), coord: self.item.coordinate(), incomingAngleDegrees: self.item.incomingAngleDegrees())
//            items.append(item)
//        }
//        performSegue(withIdentifier: "ManeuverViewController", sender: items)
    }
    
    func openManeuverVC(_ sender: Any) {
//        performManeuver(navigationUpdateTemp)
    }
    
    // MARK: - Selectors
    private func startNavigation() {
//        [[MapConfiguration sharedInstance] setMapViewType:MapViewType3d];
        self.setNavigationCameraActive(true, cancelNorthUp: false)
        self.invalidateMapSettings()
    }
    
    func stopNavigation(_ sender: UIButton?) {
//        MapConfiguration.sharedInstance().mapViewType = MapViewType2d
        hideDriveView()
//        deselectAll()
        
        navigationManager.stopGuidance()
        clearRoute()
        clearDestination()
        
        invalidateMapSettings()
    }
    
}

extension MapViewController: TTNavigationDelegate {
    func navigationManagerDestinationReached(_ manager: TTNavigationManager!) {
        self.navigationManager.stopGuidance()
    }
    
    func navigationManagerRouteAvailable(_ manager: TTNavigationManager!) {
        self.guidanceActive = true
        self.progressUtil?.cancelProgress()
        if (self.shouldStartNavigationWhenRouteReady()) {
            self.startNavigation()
        }
    }
    
    func navigationManager(_ manager: TTNavigationManager!, update: TTNavigationUpdate!) {
        guard let updateGuidance = update.guidance, let driveVC = self.driveViewController else {
            return
        }
        driveVC.updateDriveView(update , onMap: self)
        
        if (updateGuidance.routePoints() != self.lastUpdate?.guidance.routePoints()) {
            setRoutePoints(updateGuidance.routePoints(), isComplete: true, completionHandler: nil)
            if self.isInvokeOverview {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(330 * NSEC_PER_MSEC) / Double(NSEC_PER_SEC), execute: {() -> Void in
                    self.displayRouteOverview()
                })
            }
            if self.isInvokeManuevers {
                //                resultManeuverListResult(update)
                self.isInvokeManuevers = false
            }
        } else {
            self.clearCurrentRoute()
        }
        
        //if guidance active move chevron position to TTNavigationUpdate
        //else set chevron on current location
        
        if let currentLocation = CLLocationManager().location?.coordinate {
            let vehiclePos: CLLocationCoordinate2D = (guidanceActive || updateGuidance.distanceToDestination() != 0) ? update.vehiclePosition : currentLocation
            updateVehiclePosition(vehiclePos, direction: update.vehicleDirection, speed: update.speedKPH, accuracy: update.accuracyMeters, source: update.positionSource)
            lastUpdate = update
            
        }
        
    }
    
    func navigationManager(_ manager: TTNavigationManager!, routeFailedWithError error: TTGuidanceRouteError) {
        self.guidanceActive = false
        self.progressUtil?.cancelProgress()
        let alert = UIAlertController(
            title: "Error",
            message: "Cant plan route",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style: .cancel,
            handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
        print(error.rawValue)
    }
    
    func navigationManagerGPSDenied(_ manager: TTNavigationManager!) {
        self.guidanceActive = false
        manager.stopGuidance()
    }
}

extension MapViewController: UIMapViewDelegate {
    func optionButtonTag(_ tag: Int, index: Int) {
//        switch tag {
//        case MapViewOptionsTagTrafficLayer:
//            if index == 1 || index == 2 {
//                deselectOptionIndexs([2])
//            }
//            else {
//                deselectOptionIndexs([1, 0])
//            }
//            trafficLayerResult(index)
//            return
//        case MapViewOptionsTagMap2D3D:
//            resultMap2D3D(index)
//            return
//        case MapViewOptionsTagMapLayersLayer:
//            mapLayersResult(index)
//            return
//        case MapViewOptionsTagMapTilesLayer:
//            mapTilesLayerResult(index)
//            return
//        case MapViewOptionsTagCenteringMap:
//            resultCenterMap(index)
//            return
//        case MapViewOptionsTagAnnotations:
//            customMarkers(index)
//            return
//        case MapViewOptionsTagBalloons:
//            customBalloons(index)
//            return
//        case MapViewOptionsTagShapes:
//            customShapes(index)
//            return
//        case MapViewOptionsTagSpeedDependentZoom:
//            resultSpeedDependentZoomResult(index)
//            return
//        case MapViewOptionsTagRoutingWithWaypoints:
//            resultRouting(withWaypointsResult: index)
//            return
//        }
//        if multiSelect {
//            planRoute(withMultipeChoiceConfig: lastSingleConfig, masks: singleConfigMasks)
//        }
//        else {
//            planRoute(withSignleChoiceConfigsConfig: lastMultipeConfigs, index: index)
//        }
    }
}

extension MapViewController: TTMapDataLayerDelegate {
    
    func mapDataLayer(_ layer: TTMapDataLayer, itemPressed item: TTMapDataLayerItem) {
    }
}
