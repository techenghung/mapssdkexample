//
//  NavigationViewController.swift
//  MapsSDKExample
//
//  Created by 洪德晟 on 06/12/2017.
//  Copyright © 2017 洪德晟. All rights reserved.
//

import UIKit
import CoreLocation

class NavigationViewController: MapViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startABSimulation()
    }
    
    // MARK: - Start A-B Simulation
    private func startABSimulation() {
        self.center(onCoordinates:COORDINATE_AMSTERDAM)
        if let config = self.createGuidance(
            origin: COORDINATE_AMSTERDAM,
            destination: COORDINATE_ROTTERDAM,
            routeOption: TTGuidanceRouteOption(rawValue: 0),
            simulation: true,
            speed: Int32(DEFAULT_SPEED_SIMULATION)
        )
        {
            self.planRouteWithSignleChoiceConfigs([config], index: 0)
            self.showDriveView()
        }
    }

}
