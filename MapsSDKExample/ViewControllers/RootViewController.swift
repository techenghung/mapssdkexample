//
//  RootViewController.swift
//  MapsSDKExample
//
//  Created by 洪德晟 on 06/12/2017.
//  Copyright © 2017 洪德晟. All rights reserved.
//

class RootViewController: UIViewController {
    
    var dataSource: Array<Array<String>>!
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "TomTom Map Demo"
        
        let featuresTableView: UITableView = UITableView(frame: self.view.frame)
        featuresTableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        featuresTableView.delegate = self
        featuresTableView.dataSource = self
        self.view.addSubview(featuresTableView)
        
        dataSource = [["Map View"],["Navigation"]]
        
    }
}

// MARK: UITableViewDatasource

extension RootViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource[section].count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40;
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var sectionTitle: String?
        switch section {
        case 0:
            sectionTitle = "Basic Map"
        default:
            sectionTitle = "Navigation"
        }
        
        return sectionTitle
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String? = "cellIdentifier"
        var cell:UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier!)
        if cell == nil
        {
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: cellIdentifier)
        }
        
        cell!.selectionStyle = UITableViewCellSelectionStyle.none
        cell!.textLabel!.text = dataSource[indexPath.section][indexPath.row]
        
        return cell!
    }
}

// MARK: UITableViewDelegate

extension RootViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.section, indexPath.row) {
        case (0,0):
            let mapVC = UIStoryboard.main().instantiateViewController(withIdentifier:"MapViewController") as! MapViewController
            self.navigationController?.pushViewController(mapVC, animated: true)
        case (1,0) :
            let navVC = UIStoryboard.main().instantiateViewController(withIdentifier: "NavigationViewController") as! NavigationViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        default:
            break
        }
    }
}
