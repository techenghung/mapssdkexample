//
//  UIMapViewController.swift
//  MapsSDKExample
//
//  Created by 洪德晟 on 07/12/2017.
//  Copyright © 2017 洪德晟. All rights reserved.
//

import UIKit

protocol UIMapViewDelegate: NSObjectProtocol {
    func optionButtonTag(_ tag: Int, index: Int)
}

protocol NavigationProtocol: NSObjectProtocol {
    func createGuidance(_ origin: CLLocationCoordinate2D, destination dest: CLLocationCoordinate2D, routeOption: TTGuidanceRouteOption, simulation: Bool, speed: Int) -> TTGuidanceConfig
    
    func createGuidance(_ origin: CLLocationCoordinate2D, destination dest: CLLocationCoordinate2D, routeMode: TTGuidanceRouteMode, simulation: Bool, speed: Int) -> TTGuidanceConfig
    
    func planRoute(withSignleChoiceConfigsConfig configs: [TTGuidanceConfig], index: Int)
    
    func planRoute(withMultipeChoiceConfig config: TTGuidanceConfig, masks: [Any])
    
    func hideDriveView()
    
    func showDriveView()
    
    func showSearchView()
    
    func hideSearchView()
}

class UIMapViewController: TTNavViewController {
    
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var navSubtitle: UILabel!
    @IBOutlet weak var locateMeButton: UIButton!
    @IBOutlet weak var optionsContainer: UIView!
    @IBOutlet weak var option1: UIButton!
    @IBOutlet weak var option2: UIButton!
    @IBOutlet weak var option3: UIButton!
    weak var delegate: UIMapViewDelegate?
    
    var isMultiSelect = false
    var isInvokeOverview = false
    var isInvokeManuevers = false
    var isInvokeReverseGeocoderLongPress = false
    var progressUtil: ProgressUtil?
    var driveViewController: DriveViewController?
//    var searchViewController: SearchFieldsViewController?
    var currentMapOrientation: TTMapOrientation?
    var nextAction: Selector?
    var isShowToastEnable = false
    
    var toast: UIView?
    var toastTimer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        isSegmentHidden = true
    }
    
    func setTitle(_ title: String, subtitle: String) {
        navTitle.text = title
        navSubtitle.text = subtitle
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: {(_ context:  UIViewControllerTransitionCoordinatorContext) -> Void in
            self.slideDownFrameFromNavigationBarForVC(self.driveViewController!)
        }, completion: {(_ context: UIViewControllerTransitionCoordinatorContext) -> Void in
        })
    }
    
    // MARK: - UIMapViewController
    func slideDownFrameFromNavigationBarForVC(_ viewController: UIViewController) {
        guard let navigationVC = self.navigationController else {
            return
        }
        let height: CGFloat = navigationVC.navigationBar.frame.height + UIApplication.shared.statusBarFrame.height
        viewController.view.frame = CGRect(x: 0, y: height, width: self.view.frame.width, height: self.view.frame.height - height)
    }
    
    // MARK: - Buttons
    func clearSegmentedControll() {
//        isSegmentHidden = true
    }
    
    func createSegmentedControll(withTag tag: Int, options: [String], multiSelect multiselect: Bool, select: Int) {
        self.isMultiSelect = multiselect
//        segmentTag = tag
//        createSegments(options)
//        deselectAll()
//        if select >= 0 {
//            selectSegment(select, selected: true)
//        }
    }
    
    // MARK: - Option Buttton
    @IBAction func option1Changed(_ sender: UIButton) {
        optionChangedButton(sender, index: 2)
    }
    
    @IBAction func option2Changed(_ sender: UIButton) {
        optionChangedButton(sender, index: 1)
    }
    
    @IBAction func option3Chnaged(_ sender: UIButton) {
        optionChangedButton(sender, index: 0)
    }
    
    func optionChangedButton(_ button: UIButton, index: Int) {
//        if multiSelect {
//            toogleSelectSegment(index)
//        }
//        else {
//            selectSegment(index, selected: true)
//        }
//        if delegate.responds(to: Selector("optionChangedButton:Index:")) {
//            delegate.optionButtonTag(button.tag, index: index)
//        }
    }
    
    // MARK: - Internal
    func createMenuNavigationItem() {
        let menuImage = UIImage(named: "btn_menu")?.withRenderingMode(.alwaysOriginal)
        let optionsItem = UIBarButtonItem(image: menuImage, style: .plain, target: self, action: #selector(self.displayOptionsVC))
        navigationItem.leftBarButtonItem = optionsItem
    }
    
    func addNavigationButton(withAction action: Selector?, title: String, nextAction: Selector?) {
        let button = UIBarButtonItem(title: title, style: .plain, target: self, action: action)
        self.nextAction = nextAction
        navigationItem.rightBarButtonItem = button
    }
    
    @objc func displayOptionsVC(_ sender: Any) {
    }
    
    // MARK: - Show Toast
    func showToast(_ text: String) {
//        if showToastEnable {
//            if toastTimer != nil {
//                toastTimer.invalidate()
//            }
//            let interval: TimeInterval = AppDelegateInstance.isTesting() ? 5.0 : 1.25
//            toastTimer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(self.hideToast), userInfo: nil, repeats: false)
//            let style: CSToastStyle? = csToastStyle()
//            hideToast()
//            toast = view.toastView(forMessage: text, title: nil, image: nil, style: style)
//            CSToastManager.sharedStyle = style
//            CSToastManager.isTapToDismissEnabled = true
//            CSToastManager.isQueueEnabled = false
//            view.showToast(toast, duration: 60.0, position: CSToastPositionBottom) { _ in }
//        }
    }
    
    func csToastStyle() -> CSToastStyle {
        let style = CSToastStyle()
        style.backgroundColor = UIColor.white
        style.cornerRadius = 0.0
        style.messageColor = UIColor.black
//        style.displayShadow = true
        style.shadowOffset = CGSize.zero
        style.shadowOpacity = 0.3
        return style
    }
    
    func hideToast() {
        toast?.hideToast(toast!)
        toast = nil
    }

}

// MARK: - DriveViewControllerDelegate
extension UIMapViewController: DriveViewControllerDelegate {
    func centerCurrentLocationHandler() {
//                centerToCurrentLocation()
    }
    
    func hiddenLocateMeButton(_ visible: Bool) {
        //        locateMeButton.isHidden = visible
    }
    
    @IBAction func touchUpLocateMe(_ sender: Any) {
        //        centerToCurrentLocation()
    }
}

// MARK: - NavigationProtocol
extension UIMapViewController: NavigationProtocol {
    
    func createGuidance(_ origin: CLLocationCoordinate2D, destination dest: CLLocationCoordinate2D, routeOption: TTGuidanceRouteOption, simulation: Bool, speed: Int) -> TTGuidanceConfig {
        
        let config = TTGuidanceConfig(destination: dest, origin: origin)
        config?.clearAllRouteOptions()
        config?.routeOptionMask = Int32(routeOption.rawValue)
        
        config?.simulate = simulation
        config?.simulationSpeed = Int32(Int(speed))
        return config!
    }
    
    func createGuidance(_ origin: CLLocationCoordinate2D, destination dest: CLLocationCoordinate2D, routeMode: TTGuidanceRouteMode, simulation: Bool, speed: Int) -> TTGuidanceConfig {
        
        let config = TTGuidanceConfig(destination: dest, origin: origin)
        config?.clearAllRouteOptions()
        config?.routeMode = routeMode
        
        config?.simulate = simulation
        config?.simulationSpeed = Int32(Int(speed))
        return config!
    }
    
    func planRoute(withSignleChoiceConfigsConfig configs: [TTGuidanceConfig], index: Int) {
        return
    }
    
    func planRoute(withMultipeChoiceConfig config: TTGuidanceConfig, masks: [Any]) {
        return
    }
    
    func showDriveView() {
        if (self.driveViewController == nil) {
            self.driveViewController = UIStoryboard.main().instantiateViewController(withIdentifier: "DriveViewController") as? DriveViewController
            self.driveViewController?.delegate = self as? DriveViewControllerDelegate
            
            mapView.compassVisible = false
            guard let driveVC = self.driveViewController else { return }
            self.slideDownFrameFromNavigationBarForVC(driveVC)
            self.addChildViewController(driveVC)
            driveVC.view.removeFromSuperview()
            self.view.addSubview(driveVC.view)
            driveVC.didMove(toParentViewController: self)
        }
    }
    
    func hideDriveView() {
        self.driveViewController?.willMove(toParentViewController: nil)
        self.driveViewController?.view.removeFromSuperview()
        self.driveViewController?.removeFromParentViewController()
        self.driveViewController = nil
        
        mapView.compassVisible = true
    }
    
    func showSearchView() {
        //        if !searchViewController {
        //            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //            searchViewController = mainStoryboard.instantiateViewController(withIdentifier: "SearchFieldsViewController") as? SearchFieldsViewController
        //            searchViewController.naivgationProtocol = self
        //            slideDownFrameFromNavigationBar(forVC: searchViewController)
        //        }
        //        addChildViewController(searchViewController)
        //        view.addSubview(searchViewController.view)
        //        searchViewController.didMove(toParentViewController: self)
        //        navigationController?.popViewController(animated: true)
    }
    
    func hideSearchView() {
        //        searchViewController.willMove(toParentViewController: nil)
        //        searchViewController.view.removeFromSuperview()
        //        searchViewController.removeFromParentViewController()
        //        searchViewController = nil
    }
}
