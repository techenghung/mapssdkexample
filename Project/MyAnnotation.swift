//
//  MyAnnotation.swift
//  MapsSDKExample
//
//  Created by 洪德晟 on 09/12/2017.
//  Copyright © 2017 洪德晟. All rights reserved.
//

import Foundation
import MapKit

class MyAnnotation: NSObject, MKAnnotation {
    

    let coordinate: CLLocationCoordinate2D

    
    init(coordinate: CLLocationCoordinate2D) {

        self.coordinate = coordinate
    
        super.init()
    }
    
    var markerTintColor: UIColor {
        return #colorLiteral(red: 0.5559999943, green: 0.4480000138, blue: 0.3670000136, alpha: 1)
    }
    
//    static func load(by cafes: [Cafe]) -> [CafeAnnotation]? {
//        var annotations: [CafeAnnotation] = []
//
//        for cafe in cafes {
//            guard let lat = Double(cafe.latitude), let lng = Double(cafe.longitude) else {
//                return nil
//            }
//
//            let position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
//            let annotation = CafeAnnotation(type: .cafe, id: cafe.id,
//                                            title: cafe.name,
//                                            mrt: cafe.mrt,
//                                            city: cafe.city,
//                                            coordinate: position,
//                                            wifi: cafe.wifi,
//                                            seat: cafe.seat,
//                                            quiet: cafe.quiet,
//                                            tasty: cafe.tasty,
//                                            cheap: cafe.cheap,
//                                            music: cafe.music)
//            annotations.append(annotation)
//        }
//        return annotations
//    }
    
//    func mapItem() -> MKMapItem {
//        let addressDict = [CNPostalAddressStreetKey: title!]
//        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
//        let mapItem = MKMapItem(placemark: placemark)
//        mapItem.name = title
//        return mapItem
//    }
}

