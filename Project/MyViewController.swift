//
//  MyViewController.swift
//  MapsSDKExample
//
//  Created by 洪德晟 on 09/12/2017.
//  Copyright © 2017 洪德晟. All rights reserved.
//

import UIKit

class MyMapViewController: MapViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let image = createMarkerView()
        mapView.addSubview(image)
    }
    
    func createMarkerView() -> UIView {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.backgroundColor = UIColor.clear
        for _ in 0..<7 {
            let imageView = UIImageView(image: UIImage(named: "car"))
            let random1 = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
            let random2 = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
            let xCor: CGFloat = (mapView.bounds.width * random1)
            let yCor: CGFloat = (mapView.bounds.height * random2)
            print(xCor, yCor)
            imageView.frame = CGRect(x: xCor, y: yCor, width: imageView.frame.size.width, height: imageView.frame.size.height)
            view.addSubview(imageView)
        }
        return view
    }
    


}
