//
//  TTBitmap.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 1/2/14.
//  Copyright (c) 2014 Tomtom. All rights reserved.
//

#import <UIKit/UIKit.h>

struct TTBitmapImpl;

@interface TTBitmap : NSObject
{
@package
	struct TTBitmapImpl		*_bitmapImpl;
}
@property (nonatomic, readonly) CGSize size;

- (id)initWithImage:(UIImage *)image;
- (id)initWithImage:(UIImage *)image generateShadow:(int)blurRadius;
- (id)initWithViewContents:(UIView *)view;
- (id)initWithViewContents:(UIView *)view generateShadow:(int)blurRadius;

+ (TTBitmap *)bitmapWithImage:(UIImage *)image;
+ (TTBitmap *)bitmapWithImage:(UIImage *)image generateShadow:(int)blurRadius;
+ (TTBitmap *)bitmapWithViewContents:(UIView *)view;
+ (TTBitmap *)bitmapWithViewContents:(UIView *)view generateShadow:(int)blurRadius;

@end
