//
//  TTBitmapRegion.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 1/27/14.
//  Copyright (c) 2014 Tomtom. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TTBitmap;

@interface TTBitmapRegion : NSObject
{
	TTBitmap	*bitmap;
	CGRect		region;
}
@property (nonatomic, retain) TTBitmap *bitmap;
@property (nonatomic, assign) CGRect region;

- (id)initWithEntireBitmap:(TTBitmap *)bmp;
- (id)initWithBitmap:(TTBitmap *)bmp region:(CGRect)rgn;

- (BOOL)usesEntireBitmap;

+ (TTBitmapRegion *)bitmapRegionWithEntireBitmap:(TTBitmap *)bmp;
+ (TTBitmapRegion *)bitmapRegionWithBitmap:(TTBitmap *)bmp region:(CGRect)rgn;

@end
