//
//  TTConfigFile.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 12/2/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

struct TTConfigImpl;
@class TTConfigMigrationParser;

@interface TTConfigFile : NSObject
{
@package
	struct TTConfigImpl		*_configImpl;
    TTConfigMigrationParser *_parser;
}

- (instancetype)initWithConfigFile:(NSString *)filePath;

- (NSString *)valueForKey:(NSString *)key;

+ (TTConfigFile *)configFileWithFile:(NSString *)filePath __deprecated;

+ (TTConfigFile *)configFileWithDefaultPath;

@end
