#ifndef _DC_CONSTANTS_H_
#define _DC_CONSTANTS_H_

typedef enum TTLogLevel
{
	TTLogLevelNone = 0,
	TTLogLevelError,
	TTLogLevelWarn,
	TTLogLevelInfo,
	TTLogLevelDebug,
	TTLogLevelFine,
	
	TTLogLevelCount
}
TTLogLevel;

#endif	// _DC_CONSTANTS_H_
