//
//  TTFont.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 1/23/14.
//  Copyright (c) 2014 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TTFontFace;
@class TTFontOptions;
@class TTMapView;

@interface TTFont : NSObject
{
}

- (id)initWithMap:(TTMapView *)map face:(TTFontFace *)face size:(int)points options:(TTFontOptions *)options;

@end
