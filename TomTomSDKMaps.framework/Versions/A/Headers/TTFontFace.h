//
//  TTFontFace.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 1/22/14.
//  Copyright (c) 2014 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TTMapView;

@interface TTFontFace : NSObject
{
}

- (id)initWithMap:(TTMapView *)map data:(NSData *)data;
- (id)initWithMap:(TTMapView *)map data:(NSData *)data customName:(NSString *)customName;

@end
