//
//  TTFontOptions.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 1/22/14.
//  Copyright (c) 2014 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTFontOptions : NSObject
{
	float	outlineThickness;
	float	outlineOpacity;
	int		outlineOffsetX;
	int		outlineOffsetY;
	int		outlineBlurRadius;
}
@property (nonatomic, assign) float outlineThickness;
@property (nonatomic, assign) float outlineOpacity;
@property (nonatomic, assign) int outlineOffsetX;
@property (nonatomic, assign) int outlineOffsetY;
@property (nonatomic, assign) int outlineBlurRadius;

- (id)init;
- (id)initWithOutlineThickness:(float)thickness opacity:(float)opacity blurRadius:(int)blurRadius;

+ (TTFontOptions *)fontOptions;
+ (TTFontOptions *)fontOptionsWithOutlineThickness:(float)thickness opacity:(float)opacity blurRadius:(int)blurRadius;

@end
