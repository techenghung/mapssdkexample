//
//  TTFormattedText.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 1/22/14.
//  Copyright (c) 2014 Tomtom. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TTFont;

@interface TTFormattedText : NSObject
{
}
@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) TTFont *font;
@property (nonatomic, retain) UIColor *color;
@property (nonatomic, assign) float minScaleX;
@property (nonatomic, assign) float minScaleY;
@property (nonatomic, assign) int maxLines;
@property (nonatomic, assign) BOOL ellipsize;

- (id)init;
- (id)initWithString:(NSString *)string;
- (id)initWithString:(NSString *)string copyFormat:(TTFormattedText *)other;

+ (TTFormattedText *)formattedText;
+ (TTFormattedText *)formattedTextWithString:(NSString *)string;
+ (TTFormattedText *)formattedTextWithString:(NSString *)string copyFormat:(TTFormattedText *)other;

@end
