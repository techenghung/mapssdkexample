//
//  TTGuidanceAnnouncement.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 6/20/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TTGuidanceUpdate;

@protocol TTGuidanceAnnouncement <NSObject>

- (id<TTGuidanceUpdate>)getUpdate;
- (BOOL)willArrive;
- (int)announcementStage;
- (int)totalAnnouncementStages;

@end
