//
//  TTGuidanceConfig.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 5/16/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>

#import "TTGuidanceConstants.h"

/**
 Encapsulates configurations required for guidance services.
 
 @discussion These configurations cover route-related, a more general service-related configurations
 This is also where we decide if the navigation is going to be a simulation or not. 
 */
@interface TTGuidanceConfig : NSObject
{
	CLLocationCoordinate2D	destination;
	CLLocationCoordinate2D	origin;
    NSString                *routeID;
    NSString                *locale;
	TTGuidanceUnits			units;
	TTGuidanceRouteMode		routeMode;
	TTGuidanceHybridMode	routeHybridMode;
	TTGuidanceHybridMode	ttsHybridMode;
	NSString				*sensorLogPath;
    NSArray<NSString *>     *waypoints;
	int						routeOptionMask;
	BOOL					simulate;
	int						simulationSpeed;
}


/**
 A lat/long struct representing destination of a route
*/
@property (nonatomic, assign) CLLocationCoordinate2D destination;

/**
 A lat/long struct representing origin of a route
*/
@property (nonatomic, assign) CLLocationCoordinate2D origin;

/**
 Radius of distance to the closest road to the given coordinate as Origin.
 Default value is 200 m.
 */
@property (nonatomic, assign) double snapOriginDistance;

/**
 Radius of distance to the closest road to the given coordinate as Destination.
 Default value is 200 m.
 */
@property (nonatomic, assign) double snapDestinationDistance;

/**
 Radius of threshold to Origin coordinate. 
 Default value is 100m.
 */
@property (nonatomic, assign) double snapOriginThreshold;

/**
 The routeID is a string that is a Base64 encoded value return from a backend route request
*/
@property (nonatomic, assign) NSString *routeID;

/**
 The locale is tuple of two strings representing country and language eg. en-US
 */
@property (nonatomic, assign) NSString *locale;

/**
 Units used in guidance instructions – either Imperial or Metric
*/
@property (nonatomic, assign) TTGuidanceUnits units;

/**
 @discussion Enumeration of route modes:<br>
 -Car<br>
 -Pedestrial<br>
 -Commercial<br>
 -Carpool<br>
*/
@property (nonatomic, assign) TTGuidanceRouteMode routeMode;

/**
 This proprety will be used to determine the mode of the route, weather it's online, offline, or hybrid.
 */
@property (nonatomic, assign) TTGuidanceHybridMode routeHybridMode;

/**
 This proprety will be used to determine the mode of the TTS, weather it's online, offline, or hybrid.
 */
@property (nonatomic, assign) TTGuidanceHybridMode ttsHybridMode;

/**
 Represents a path at which sensor log will be saved
 */
@property (nonatomic, retain) NSString *sensorLogPath;

/**
 List of waypoints to be considered when planning the route
 NSArray of NSString which encapsulate CLLocationCoordinate2D
 e.i. @[@"51.752407,19.571013", @"51.13527,19.5673"]
 */
@property (nonatomic, retain) NSArray<NSString *> *waypoints;

/**
 An options mask used to add, remove, query for options from TTGuidanceRouteOption.
 */
@property (nonatomic, assign) int routeOptionMask;

/**
 A flag to determine if the simulation behavior should take place instead of a real navigation.
 */
@property (nonatomic, assign) BOOL simulate;

/**
 Determines a speed of a vehicle in the simulation.
 */
@property (nonatomic, assign) int simulationSpeed;

/**
 Determine if we should use NKW as the routing engine.
 */
@property (nonatomic, assign) BOOL useNKW;

/**
 initializer with a route destination point.
 
 @discussion Destination coordinate is required. accepts a destination point of lat/lon. Origin coordinate is required if simulate == YES. It is optional for GPS or sensorlog playback.
 @param aDestination The lat/long representation of the route destination.
 @return id The newely constructed object
*/
- (id)initWithDestination:(CLLocationCoordinate2D)aDestination;


/**
 initializer with the route's destination and origin points.
 @discussion Destination coordinate is required. accepts a destination point of lat/lon.
 Origin coordinate is required if simulate == YES. It is optional for GPS or sensorlog playback.
 Best practice is to only supply an origin coordinate if it is of maximum accuracy (i.e. from GPS) in order to
 minimize the likelihood of immediate rerouting.
 @param aDestination The lat/long representation of the route destination.
 @param anOrigin The lat/long representation of the route origin.
 @return id The newely constructed object
*/
- (id)initWithDestination:(CLLocationCoordinate2D)aDestination origin:(CLLocationCoordinate2D)anOrigin;

/**
 routeID setter
 @param aRouteID The routeID is a string that is a Base64 encoded value return from a backend route request.
*/
- (void)setRouteID:(NSString *)aRouteID;

/**
 set an option from TTGuidanceRouteOption of the mask of options maintained by this object
 @param option an option from TTGuidanceRouteOption enumeration.
*/
- (void)setRouteOption:(TTGuidanceRouteOption)option;

/**
 clears an option from TTGuidanceRouteOption of the mask of options maintained by this object
 @param option an option from TTGuidanceRouteOption enumeration.
*/
- (void)clearRouteOption:(TTGuidanceRouteOption)option;

/**
 clears all the routing options.
*/
- (void)clearAllRouteOptions;

/**
 queries the options mask if it contains a specific option
 @param option an option from TTGuidanceRouteOption enumeration.
 @return BOOL determines if the option is set or not.
*/
- (BOOL)isRouteOptionSet:(TTGuidanceRouteOption)option;

/**
 factory method with destination point.
 @param aDestination CLLocationCoordinate2D lat/lon of destination point
 @return TTGuidanceConfig * reference to the custructed object
*/
+ (TTGuidanceConfig *)configWithDestination:(CLLocationCoordinate2D)aDestination;

/**
 factory method with destination point.
 @param aDestination CLLocationCoordinate2D lat/lon of destination point
 @param anOrigin CLLocationCoordinate2D lat/lon of origin point
 @return TTGuidanceConfig * reference to the custructed object
*/
+ (TTGuidanceConfig *)configWithDestination:(CLLocationCoordinate2D)aDestination origin:(CLLocationCoordinate2D)anOrigin;
@end
