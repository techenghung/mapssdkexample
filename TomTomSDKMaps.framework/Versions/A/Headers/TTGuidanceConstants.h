#ifndef _DCGUIDANCE_CONSTANTS_H_
#define _DCGUIDANCE_CONSTANTS_H_

typedef enum TTGuidanceUnits
{
	TTGuidanceUnitsImperial = 0,
	TTGuidanceUnitsMetric,
	
	TTGuidanceUnitsCount
}
TTGuidanceUnits;

typedef enum TTGuidanceRouteMode
{
	TTGuidanceRouteModeCar = 0,
	TTGuidanceRouteModePedestrian,
	TTGuidanceRouteModeCommercial,
	TTGuidanceRouteModeCarpool,
	
	TTGuidanceRouteModeCount
}
TTGuidanceRouteMode;

typedef enum TTGuidanceLanguage {
    TTGuidanceLanguageEnglish = 0,
    TTGuidanceLanguageSpanish,
    TTGuidanceLanguageFrench,
    TTGuidanceLanguageGerman,
    
    TTGuidanceLanguageCount
}
TTGuidanceLanguage;

typedef enum TTGuidanceRouteOption
{
	TTGuidanceRouteOptionAvoidFerry = 0,
	TTGuidanceRouteOptionAvoidTunnel,
	TTGuidanceRouteOptionAvoidBridge,
	TTGuidanceRouteOptionAvoidFreeway,
	TTGuidanceRouteOptionAvoidToll,
	TTGuidanceRouteOptionEnableTraffic,
	TTGuidanceRouteOptionFastest,
	TTGuidanceRouteOptionShortest,
	TTGuidanceRouteOptionEasiest,
    TTGuidanceRouteOptionNKW = 9,
    TTGuidanceRouteOptionFixedTime = 10,
    TTGuidanceRouteOptionPedestrian = 11,

	TTGuidanceRouteOptionCount
}
TTGuidanceRouteOption;

typedef enum TTGuidanceHybridMode
{
	TTGuidanceHybridModeOnboard = 0,
	TTGuidanceHybridModeOffboard,
	TTGuidanceHybridModeHybrid,
	
	TTGuidanceHybridModeCount
}
TTGuidanceHybridMode;

typedef enum TTGuidanceIconType
{
	TTGuidanceIconTypeIntersection = 0,
	TTGuidanceIconTypeRoundaboutRight,
	TTGuidanceIconTypeRoundaboutLeft,
	TTGuidanceIconTypeKeepStraight,
	TTGuidanceIconTypeHighwayManeuverRight,
	TTGuidanceIconTypeHighwayManeuverLeft,
	TTGuidanceIconTypeUTurnRight,
	TTGuidanceIconTypeUTurnLeft,
	
	TTGuidanceIconTypeCount
}
TTGuidanceIconType;

typedef enum TTGuidanceSimpleIconDirection
{
	TTGuidanceSimpleIconDirectionReverse = 0,
	TTGuidanceSimpleIconDirectionSharpLeft,
	TTGuidanceSimpleIconDirectionLeft,
	TTGuidanceSimpleIconDirectionKeepLeft,
	TTGuidanceSimpleIconDirectionStraightAhead,
	TTGuidanceSimpleIconDirectionKeepRight,
	TTGuidanceSimpleIconDirectionRight,
	TTGuidanceSimpleIconDirectionSharpRight,
	
	TTGuidanceSimpleIconDirectionCount
}
TTGuidanceSimpleIconDirection;

typedef enum TTGuidancePositionSource
{
	TTGuidancePositionSourceHardware,
	TTGuidancePositionSourceExtrapolated,
	TTGuidancePositionSourceUnknown,
	
	TTGuidancePositionSourceCount
}
TTGuidancePositionSource;

typedef enum TTGuidanceSetupError
{
	TTGuidanceSetupErrorFailedRegistrationNetwork,
	TTGuidanceSetupErrorFailedRegistrationAccessDenied,
	TTGuidanceSetupErrorFailedRegistrationNotAuthorized,
	TTGuidanceSetupErrorFailedRegistrationProtocolError,
	TTGuidanceSetupErrorFailedRegistrationServerError,
	
	TTGuidanceSetupErrorCount
}
TTGuidanceSetupError;

typedef enum TTGuidanceRouteError
{
	TTGuidanceRouteErrorFailedNetwork,
	TTGuidanceRouteErrorFailedAccessDenied,
	TTGuidanceRouteErrorFailedNotAuthorized,
	TTGuidanceRouteErrorFailedInvalidOrigin,
	TTGuidanceRouteErrorFailedInvalidDestination,
	TTGuidanceRouteErrorFailedInvalidRoute,
	TTGuidanceRouteErrorFailedProtocolError,
	TTGuidanceRouteErrorFailedServerError,
	TTGuidanceRouteErrorOriginSnapped,
	TTGuidanceRouteErrorDestinationSnapped,
    TTGuidanceRouteErrorUndefinedError,
    TTGuidanceRouteErrorMaxConcurrentQueriesExceeded,
    TTGuidanceRouteErrorServiceShuttingDown,
    TTGuidanceRouteErrorIdAlreadyExists,
    TTGuidanceRouteErrorMapMatchingFailure,
    TTGuidanceRouteErrorRouteFound,
    TTGuidanceRouteErrorStopped,
    TTGuidanceRouteErrorBadInput,
    TTGuidanceRouteErrorCannotRestoreBaseroute,
    TTGuidanceRouteErrorGuidanceError,
	
	TTGuidanceRouteErrorCount
}
TTGuidanceRouteError;


#endif	// _DCGUIDANCE_CONSTANTS_H_
