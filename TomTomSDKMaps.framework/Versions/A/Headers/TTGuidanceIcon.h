//
//  TTGuidanceIcon.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 6/3/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TTGuidanceConstants.h"

@protocol TTGuidanceSimpleIcon;

@protocol TTGuidanceIcon <NSObject>

- (TTGuidanceIconType)iconType;
- (int)legsMask;
- (int)exitLeg;
- (int)roundaboutExitNumber;
- (id<TTGuidanceSimpleIcon>)simpleIcon;
- (BOOL)isEqualToIcon:(id<TTGuidanceIcon>)icon;

+ (float)angleForLeg:(int)leg;

@end
