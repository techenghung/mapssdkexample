//
//  TTGuidanceLane.h
//  TomTomSDKMaps
//
//  Created by James McMullen 11 oct 2014.
//  Copyright (c) 2014 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol TTGuidanceLane <NSObject>
- (NSArray *) laneGuidanceScores;
@end
