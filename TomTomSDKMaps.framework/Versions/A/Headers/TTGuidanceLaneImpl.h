//
//  TTGuidanceLaneImpl.h
//  TomTomSDKMaps
//
//  Created by James McMullen 11 oct 2014.
//  Copyright (c) 2014 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTGuidanceLane.h"

@protocol ComDecartaGuidanceUserLaneGuidance;

@interface TTGuidanceLaneImpl : NSObject <TTGuidanceLane>
{
@public
    id<ComDecartaGuidanceUserLaneGuidance> guidanceUserLaneGuidance;
}

- (id)initWithUserLaneGuidance:(id<ComDecartaGuidanceUserLaneGuidance>)aGuidanceUserLaneGuidance;
- (void)dealloc;
@end
