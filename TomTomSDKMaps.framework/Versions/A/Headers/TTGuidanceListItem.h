//
//  DCDirectionsListItem.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 5/21/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol TTGuidanceIcon;

@protocol TTGuidanceListItem <NSObject>

- (NSString *)description;
- (id<TTGuidanceIcon>)icon;
- (CLLocationCoordinate2D)coordinate;
- (float)incomingAngleDegrees;	// angle is clockwise from North = 0
- (float)outgoingAngleDegrees;	// angle is clockwise from North = 0
- (int)distance;
- (BOOL)isDestination;
- (BOOL)isEqual:(id<TTGuidanceListItem>)other;
- (BOOL)isWaypoint;

@end
