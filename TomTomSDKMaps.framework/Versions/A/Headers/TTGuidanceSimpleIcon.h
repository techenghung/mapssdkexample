//
//  TTGuidanceIcon.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 5/22/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TTGuidanceConstants.h"

@protocol TTGuidanceSimpleIcon <NSObject>

- (TTGuidanceSimpleIconDirection)simpleDirection;
- (BOOL)isRoundabout;

@end
