//
//  TTGuidanceUpdate.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 5/16/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//


#import <Foundation/Foundation.h>

@protocol TTGuidanceIcon;
@protocol TTGuidanceLane;

/**
 Protocol of guidance updates, an object implementing this protocol is returned as part of the TTNavigationUpdate, contains a snapshot of the position and guidance information
 If we have an active route, several guidance methods are applied to the position with the help of a positioning source.
 */
@protocol TTGuidanceUpdate <NSObject>

- (NSString *)currentStreet;
- (NSString *)nextStreet;
- (id<TTGuidanceIcon>)maneuverIcon;
- (NSData *)routePoints;
- (BOOL)isRouteComplete;
- (NSArray *)directionsList;
- (int)distanceToCrossing;
- (int)distanceToDestination;
- (int)secondsToArrival;
- (int)speedLimit;
- (BOOL)isSpeedSignStyleUS;
- (BOOL)isSpeedLimitUnitMPH;
- (int)currentSpeedInKMH;
- (id<TTGuidanceLane>)guidanceLane;
@end
