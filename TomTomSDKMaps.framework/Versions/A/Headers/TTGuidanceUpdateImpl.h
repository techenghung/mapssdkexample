//
//  TTGuidanceUpdateImpl.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 5/21/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TTGuidanceAnnouncement.h"
#import "TTGuidanceUpdate.h"
#import "TTIconImpl.h"

@class ComDecartaGuidanceUserRoute;
@protocol ComDecartaGuidanceGuidanceInfo;

@interface TTGuidanceUpdateImpl : TTIconImpl <TTGuidanceUpdate, TTGuidanceAnnouncement>
{
@public
	id<ComDecartaGuidanceGuidanceInfo>	guidanceInfo;
	ComDecartaGuidanceUserRoute			*routeSnapshot;
	NSData								*routePoints;
	NSArray								*directionsList;
	BOOL								isRouteComplete;
}

- (id)initWithGuidanceInfo:(id<ComDecartaGuidanceGuidanceInfo>)aGuidanceInfo previousUpdate:(TTGuidanceUpdateImpl *)previous;

@end
