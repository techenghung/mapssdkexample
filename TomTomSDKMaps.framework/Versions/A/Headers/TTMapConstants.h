#ifndef _DCMAP_CONSTANTS_H_
#define _DCMAP_CONSTANTS_H_

#import <CoreLocation/CoreLocation.h>

typedef enum TTMapHybridMode
{
	TTMapHybridModeOnboard = 0,
	TTMapHybridModeOffboard,
	TTMapHybridModeHybrid,
	
	TTMapHybridModeCount
}
TTMapHybridMode;

typedef enum TileFormatConfiguration
{
    TileFormatConfigurationVectorTiles,
    TileFormatConfigurationRasterTiles,
    TileFormatConfigurationMixedTiles,
    TileFormatConfigurationInvalid
} TileFormatConfiguration;

struct TTBoundingBox {
    CLLocationCoordinate2D topLeft;
    CLLocationCoordinate2D bottomRight;
};

typedef struct TTBoundingBox TTBoundingBox;

static TTBoundingBox TTBoundingBoxMake(CLLocationCoordinate2D topLeft, CLLocationCoordinate2D bottomRigth)
{
    TTBoundingBox  boundingBox;
    boundingBox.topLeft = topLeft;
    boundingBox.bottomRight = bottomRigth;
    return boundingBox;
}

#endif	// _DCMAP_CONSTANTS_H_
