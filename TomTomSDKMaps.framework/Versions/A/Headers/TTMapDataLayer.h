//
//  TTMapDataLayer.h
//  TomTomSDKMaps
//
//  Created by Kapica on 10/03/16.
//  Copyright © 2016 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TTMapDataLayer;
@class TTMapDataLayerItem;

@protocol TTMapDataLayerDelegate <NSObject>
@optional
-(void)mapDataLayer:(TTMapDataLayer*)layer itemAdded:(TTMapDataLayerItem*)item;
-(void)mapDataLayer:(TTMapDataLayer*)layer itemRemoved:(TTMapDataLayerItem*)item;
-(void)mapDataLayer:(TTMapDataLayer*)layer itemPressed:(TTMapDataLayerItem*)item;
-(void)mapDataLayer:(TTMapDataLayer*)layer visibilityWillChange:(BOOL)hide;
@end

@interface TTMapDataLayer : NSObject
@property (nonatomic, assign) id<TTMapDataLayerDelegate> delegate;

- (void)addItem:(TTMapDataLayerItem*)item;
- (void)removeItem:(TTMapDataLayerItem*)item;

@end
