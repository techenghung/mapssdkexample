//
//  TTMapDataLayerItem.h
//  TomTomSDKMaps
//
//  Created by Kapica on 10/03/16.
//  Copyright © 2016 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@class TTMapDataLayerItem;

typedef void (^DCUPixPositionChangedFn)(const CGPoint position);

@protocol TTMapDataLayerItemDelegate <NSObject>
@optional
-(void)mapDataLayerItem:(TTMapDataLayerItem*)item uPixPositionChanged:(CGPoint)position;
@end

@interface TTMapDataLayerItem : NSObject

@property (nonatomic, assign) BOOL visible;
@property (nonatomic, readonly) CGPoint uPix;
@property (nonatomic, assign) id<TTMapDataLayerItemDelegate> delegate;

// Returns an autoreleased object. Retain the returned object to keep receiving updates, release it when you are done.
- (id)registerForUPixPositionUpdatesWithCallback:(DCUPixPositionChangedFn)callback;
@end
