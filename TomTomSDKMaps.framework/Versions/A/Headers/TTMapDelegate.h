/*
 *  TTMapDelegate.h
 *  Sunder
 *
 *  Created by Daniel Posluns on 2/25/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#import <CoreLocation/CoreLocation.h>

@class TTMapOrientation;
@class TTMapView;

@protocol TTMapDelegate <NSObject>

@optional
- (void)loadComponents:(TTMapView *)mapView;
- (void)doneLoading:(TTMapView *)mapView;
- (void)dcMap:(TTMapView *)mapView viewChangedToLatLon:(CLLocationCoordinate2D)focusLatLon zoomLevel:(int)zoomLevel perspectiveAmount:(float)ratio yaw:(float)degrees;
- (void)dcMap:(TTMapView *)mapView zoomLevelChanged:(int)toZoomLevel canZoomIn:(BOOL)zoomIn canZoomOut:(BOOL)zoomOut;
- (void)dcMap:(TTMapView *)mapView tapAtLatLon:(CLLocationCoordinate2D)coord;
- (void)dcMap:(TTMapView *)mapView longPressAtLatLon:(CLLocationCoordinate2D)coord;
- (void)dcMap:(TTMapView *)mapView doneAnimatingToOrientation:(TTMapOrientation *)orientation;

@end
