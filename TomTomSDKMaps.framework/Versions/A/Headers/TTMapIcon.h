//
//  TTMapIcon.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 1/2/14.
//  Copyright (c) 2014 Tomtom. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreLocation/CoreLocation.h>

@class TTBitmap;
@class TTBitmapRegion;
@class TTMapIcon;
@class TTMapView;
@protocol TTMapIconDelegate;

typedef enum TTMapIconType
{
	TTMapIconTypeIcon = 0,
	TTMapIconTypeBillboard,
	TTMapIconTypeStanding,
	TTMapIconTypeDecal,
	TTMapIconTypeDecalBillboard,
	
	TTMapIconTypeCount
}
TTMapIconType;

@interface TTMapIcon : NSObject
{
}
@property (nonatomic, readonly) TTMapView *owningMap;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly) TTMapIconType iconType;
@property (nonatomic, readonly) CGPoint attachmentViewPosition;
@property (nonatomic, assign) BOOL visible;
@property (nonatomic, assign) id<TTMapIconDelegate> delegate;
@property (nonatomic, assign) CGRect bounds;
@property (nonatomic, assign) CGPoint anchorCoordinate;
@property (nonatomic, assign) float scale;
@property (nonatomic, retain) UIColor *color;
@property (nonatomic, assign) BOOL shadowVisible;
@property (nonatomic, retain) UIColor *shadowColor;
@property (nonatomic, assign) BOOL deviceScalingEnabled;

- (id)initWithMap:(TTMapView *)map type:(TTMapIconType)type location:(CLLocationCoordinate2D)coord image:(UIImage *)img;
- (id)initWithMap:(TTMapView *)map type:(TTMapIconType)type location:(CLLocationCoordinate2D)coord image:(UIImage *)img shadowBlur:(int)radius;
- (id)initWithMap:(TTMapView *)map type:(TTMapIconType)type location:(CLLocationCoordinate2D)coord bitmap:(TTBitmap *)bmp;
- (id)initWithMap:(TTMapView *)map type:(TTMapIconType)type location:(CLLocationCoordinate2D)coord bitmap:(TTBitmap *)bmp region:(CGRect)region;
- (id)initWithMap:(TTMapView *)map type:(TTMapIconType)type location:(CLLocationCoordinate2D)coord bitmapRegion:(TTBitmapRegion *)bmpRegion;
- (void)setVisible:(BOOL)visible animated:(BOOL)animated orderDelay:(int)orderDelay;
- (void)killWithAnimation:(BOOL)animated;

// Fetch the position of a point on the icon in the containing TTMapView.
// Ratios are 0.0 to 1.0 ranging from top-left to bottom-right.
// For example, to get the top-center of an icon you would call viewPositionForIconRatios:CGPointMake(0.5, 0.0).
- (CGPoint)viewPositionForIconCoordinate:(CGPoint)coordinate;
- (CGPoint)viewPositionForIconRatios:(CGPoint)ratios;

@end
