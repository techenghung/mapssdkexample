//
//  TTMapIconDelegate.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 6/24/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TTMapIcon;

@protocol TTMapIconDelegate <NSObject>

@optional
- (void)iconSelected:(TTMapIcon *)icon;

@end
