//
//  TTMapLaunchState.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 4/29/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "TTMapConstants.h"

@class TTConfigFile;
@class TTMapServerConfig;
@class TTMapOrientation;

@interface TTMapLaunchState : NSObject
{
	TTMapServerConfig		*tileConfig;
	TTMapServerConfig		*trafficConfig;
    TTMapServerConfig		*trafficIncidentsConfig;
	TTMapServerConfig		*satelliteConfig;
    TTMapServerConfig		*satelliteHybridConfig;
    TTMapServerConfig		*staticAlertConfig;
    TTMapServerConfig		*dynamicAlertConfig;
    TTMapServerConfig		*poiConfig;
	TTMapServerConfig		*wsConfig;
	NSString				*mapProperties;
	NSString				*configProperties;
	NSString				*loggingProperties;
	TTMapOrientation		*startOrientation;
    TTMapHybridMode         hybridMode;
    TileFormatConfiguration tileFormatConfiguration;
}

@property (nonatomic, retain) TTMapServerConfig *tileConfig;
@property (nonatomic, retain) TTMapServerConfig *trafficConfig;
@property (nonatomic, retain) TTMapServerConfig *trafficIncidentsConfig;
@property (nonatomic, retain) TTMapServerConfig *satelliteConfig;
@property (nonatomic, retain) TTMapServerConfig *satelliteHybridConfig;
@property (nonatomic, retain) TTMapServerConfig *staticAlertConfig;
@property (nonatomic, retain) TTMapServerConfig *dynamicAlertConfig;
@property (nonatomic, retain) TTMapServerConfig *poiConfig;
@property (nonatomic, retain) TTMapServerConfig *wsConfig;
@property (nonatomic, retain) NSString *mapProperties;
@property (nonatomic, retain) NSString *configProperties;
@property (nonatomic, retain) NSString *loggingProperties;
@property (nonatomic, retain) TTMapOrientation *startOrientation;
@property (nonatomic, assign) TTMapHybridMode hybridMode;
@property (nonatomic, assign) TileFormatConfiguration tileFormatConfiguration;

- (id)init;
- (id)initWithConfigFile:(TTConfigFile *)config;
- (id)initWithStartOrientation:(TTMapOrientation *)orientation;
- (id)initWithConfigFile:(TTConfigFile *)config startOrientation:(TTMapOrientation *)orientation;
- (id)initWithConfigFile:(TTConfigFile *)config startOrientation:(TTMapOrientation *)orientation hybridMode:(TTMapHybridMode)hybridMode;

+ (TTMapHybridMode)defaultHybridMode;

@end
