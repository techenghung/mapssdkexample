//
//  TTMapOrientation+TTMapView.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 9/13/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

#import "TTMapOrientation.h"

@class TTMapView;

@interface TTMapOrientation (TTMapView)

- (id)initWithRegionCenter:(CLLocationCoordinate2D)center spanLatitude:(double)latitude spanLongitude:(double)longitude inView:(TTMapView *)view;
- (id)initWithRegionNorthWest:(CLLocationCoordinate2D)northWest southEast:(CLLocationCoordinate2D)southEast inView:(TTMapView *)view;

+ (TTMapOrientation *)orientationWithRegionCenter:(CLLocationCoordinate2D)center spanLatitude:(double)latitude spanLongitude:(double)longitude inView:(TTMapView *)view;
+ (TTMapOrientation *)orientationWithRegionNorthWest:(CLLocationCoordinate2D)northWest southEast:(CLLocationCoordinate2D)southEast inView:(TTMapView *)view;

@end
