//
//  TTMapOrientation.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 9/13/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface TTMapOrientation : NSObject
{
	CLLocationCoordinate2D	focusCoordinate;
	int						zoomLevel;
	float					perspectiveRatio;
	float					yawDegrees;
	int						usageMask;
}
@property (nonatomic, assign) CLLocationCoordinate2D focusCoordinate;
@property (nonatomic, assign) int zoomLevel;
@property (nonatomic, assign) float perspectiveRatio;
@property (nonatomic, assign) float yawDegrees;

- (id)init;
- (id)initWithFocusCoordinate:(CLLocationCoordinate2D)coordinate;
- (id)initWithFocusCoordinate:(CLLocationCoordinate2D)coordinate zoomLevel:(int)zoom;
- (id)initWithFocusCoordinate:(CLLocationCoordinate2D)coordinate zoomLevel:(int)zoom perspectiveRatio:(float)ratio;
- (id)initWithFocusCoordinate:(CLLocationCoordinate2D)coordinate zoomLevel:(int)zoom perspectiveRatio:(float)ratio yawDegrees:(float)degrees;
- (id)initOverheadNorthupWithFocusCoordinate:(CLLocationCoordinate2D)coordinate zoomLevel:(int)zoom;

+ (TTMapOrientation *)orientation;
+ (TTMapOrientation *)orientationWithFocusCoordinate:(CLLocationCoordinate2D)coordinate;
+ (TTMapOrientation *)orientationWithFocusCoordinate:(CLLocationCoordinate2D)coordinate zoomLevel:(int)zoom;
+ (TTMapOrientation *)orientationWithFocusCoordinate:(CLLocationCoordinate2D)coordinate zoomLevel:(int)zoom perspectiveRatio:(float)ratio;
+ (TTMapOrientation *)orientationWithFocusCoordinate:(CLLocationCoordinate2D)coordinate zoomLevel:(int)zoom perspectiveRatio:(float)ratio yawDegrees:(float)degrees;
+ (TTMapOrientation *)orientationOverheadNorthupWithFocusCoordinate:(CLLocationCoordinate2D)coordinate zoomLevel:(int)zoom;

- (void)setFocusCoordinate:(CLLocationCoordinate2D)coordinate;
- (void)setZoomLevel:(int)zoom;
- (void)setPerspectiveRatio:(float)ratio;
- (void)setYawDegrees:(float)degrees;

- (void)clearFocusCoordinate;
- (void)clearZoomLevel;
- (void)clearPerspectiveRatio;
- (void)clearYawDegrees;

- (BOOL)hasFocusCoordinate;
- (BOOL)hasZoomLevel;
- (BOOL)hasPerspectiveRatio;
- (BOOL)hasYawDegrees;

@end
