//
//  TTMapPopup.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 1/23/14.
//  Copyright (c) 2014 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class TTMapIcon;
@class TTMapPopupLayout;
@class TTMapPushpin;
@class TTMapView;

@interface TTMapPopup : NSObject
{
}

- (id)initWithMap:(TTMapView *)map text:(NSString *)text;
- (id)initWithMap:(TTMapView *)map mainText:(NSString *)mainText subText:(NSString *)subText;
- (id)initWithMap:(TTMapView *)map layout:(TTMapPopupLayout *)layout;

- (void)attachToPin:(TTMapPushpin *)pin;
- (void)attachToIcon:(TTMapIcon *)icon;
- (void)setLocation:(CLLocationCoordinate2D)coordinate;
- (void)setNextLayout:(TTMapPopupLayout *)layout animated:(BOOL)animated;

+ (TTMapPopup *)popupWithMap:(TTMapView *)map text:(NSString *)text;
+ (TTMapPopup *)popupWithMap:(TTMapView *)map mainText:(NSString *)mainText subText:(NSString *)subText;
+ (TTMapPopup *)popupWithMap:(TTMapView *)map layout:(TTMapPopupLayout *)layout;

@end
