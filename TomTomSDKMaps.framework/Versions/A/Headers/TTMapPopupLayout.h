//
//  TTMapPopupLayout.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 1/27/14.
//  Copyright (c) 2014 Tomtom. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TTBitmap;
@class TTBitmapRegion;
@class TTFormattedText;

typedef void (^TTPopupCallback)();

typedef enum DCSizeRestrictMode
{
	DCSizeRestrictModeNone = 0,
	DCSizeRestrictModeCurrentOrientation,
	DCSizeRestrictModeAnyOrientation,
	DCSizeRestrictModeCustomMaxSize,
	
	DCSizeRestrictModeCount,
	DCSizeRestrictModeDefault = DCSizeRestrictModeAnyOrientation
}
DCSizeRestrictMode;

typedef enum TTPopupFrameMode
{
	TTPopupFrameModeBest = 0,
	TTPopupFrameModeCenter,
	
	TTPopupFrameModeCount,
	TTPopupFrameModeDefault = TTPopupFrameModeBest
}
TTPopupFrameMode;

typedef enum DCVerticalAlignment
{
	DCVerticalAlignmentTop = 0,
	DCVerticalAlignmentMiddle,
	DCVerticalAlignmentBottom,
	
	DCVerticalAlignmentCount,
	DCVerticalAlignmentDefault = DCVerticalAlignmentMiddle
}
DCVerticalAlignment;

@interface TTMapPopupLayout : NSObject
{
	TTFormattedText			*mainText;
	TTFormattedText			*subText;
	TTBitmapRegion			*mainTextImage;
	TTBitmapRegion			*subTextImage;
	TTBitmapRegion			*leftAccessory;
	TTBitmapRegion			*rightAccessory;
	TTPopupCallback			displayTouchCallback;
	TTPopupCallback			leftAccessoryTouchCallback;
	TTPopupCallback			rightAccessoryTouchCallback;
	DCSizeRestrictMode		sizeRestrictMode;
	TTPopupFrameMode		frameMode;
	DCVerticalAlignment		textVerticalAlignment;
	CGSize					customMaxSize;
}
@property (nonatomic, retain) TTFormattedText *mainText;
@property (nonatomic, retain) TTFormattedText *subText;
@property (nonatomic, retain) TTBitmapRegion *mainTextImage;
@property (nonatomic, retain) TTBitmapRegion *subTextImage;
@property (nonatomic, retain) TTBitmapRegion *leftAccessory;
@property (nonatomic, retain) TTBitmapRegion *rightAccessory;
@property (nonatomic, copy) TTPopupCallback displayTouchCallback;
@property (nonatomic, copy) TTPopupCallback leftAccessoryTouchCallback;
@property (nonatomic, copy) TTPopupCallback rightAccessoryTouchCallback;
@property (nonatomic, assign) DCSizeRestrictMode sizeRestrictMode;
@property (nonatomic, assign) TTPopupFrameMode frameMode;
@property (nonatomic, assign) DCVerticalAlignment textVerticalAlignment;
@property (nonatomic, assign) CGSize customMaxSize;

- (id)init;
- (id)initWithText:(NSString *)text;
- (id)initWithFormattedText:(TTFormattedText *)text;
- (id)initWithMainText:(NSString *)text subText:(NSString *)subText;
- (id)initWithFormattedMainText:(TTFormattedText *)mainText subText:(TTFormattedText *)subText;

- (void)setLeftAccessory:(TTBitmap *)leftAccessory callback:(TTPopupCallback)callback;
- (void)setLeftAccessory:(TTBitmap *)leftAccessory region:(CGRect)region callback:(TTPopupCallback)callback;
- (void)setLeftAccessoryRegion:(TTBitmapRegion *)leftAccessory callback:(TTPopupCallback) callback;

- (void)setRightAccessory:(TTBitmap *)rightAccessory callback:(TTPopupCallback)callback;
- (void)setRightAccessory:(TTBitmap *)rightAccessory region:(CGRect)region callback:(TTPopupCallback)callback;
- (void)setRightAccessoryRegion:(TTBitmapRegion *)rightAccessory callback:(TTPopupCallback) callback;

+ (TTMapPopupLayout *)layout;
+ (TTMapPopupLayout *)layoutWithText:(NSString *)text;
+ (TTMapPopupLayout *)layoutWithFormattedText:(TTFormattedText *)text;
+ (TTMapPopupLayout *)layoutWithMainText:(NSString *)mainText subText:(NSString *)subText;
+ (TTMapPopupLayout *)layoutWithFormattedMainText:(TTFormattedText *)mainText subText:(TTFormattedText *)subText;

@end
