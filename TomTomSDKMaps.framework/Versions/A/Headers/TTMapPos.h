//
//  TTMapPos.h
//  TomTomSDKMaps
//
//  Created by Kapica on 11/03/16.
//  Copyright © 2016 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

typedef void (^DCViewPositionChangedFn)(const CGPoint position);
typedef void (^DCScreenPositionChangedFn)(const CGPoint position);
typedef void (^DCUIPositionChangedFn)(const CGPoint position);

@interface TTMapPos : NSObject
@property (nonatomic, assign) CGPoint uPix;
@property (nonatomic, assign) CGPoint viewPos;
@property (nonatomic, readonly) CGPoint screenPos;
@property (nonatomic, readonly) CGPoint uiPos;

- (id)registerForViewPositionUpdatesWithCallback:(DCViewPositionChangedFn)callback;
- (id)registerForScreenPositionUpdatesWithCallback:(DCScreenPositionChangedFn)callback;
- (id)registerForUIPositionUpdatesWithCallback:(DCUIPositionChangedFn)callback;
@end
