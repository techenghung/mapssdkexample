//
//  TTMapPushpin.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 6/24/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreLocation/CoreLocation.h>

@class TTMapPushpin;
@class TTMapPushpinImpl;
@class TTMapView;
@protocol TTMapPushpinDelegate;

typedef void (^DCPushpinUpdateFn)(TTMapPushpin *);

@interface TTMapPushpin : NSObject
{
@package
	TTMapPushpinImpl	*_pushpinImpl;
}
@property (nonatomic, readonly) TTMapView *owningMap;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly) CGPoint viewHeadPosition;
@property (nonatomic, assign) id<TTMapPushpinDelegate> delegate;
@property (nonatomic, retain) UIColor *headColor;
@property (nonatomic, assign) BOOL visible;

- (id)initWithMap:(TTMapView *)mapView location:(CLLocationCoordinate2D)coord initiallyVisible:(BOOL)visible;
- (void)animateEntry;
- (void)animateEntryWithOrderDelay:(int)orderDelay;
- (void)setVisible:(BOOL)visible animated:(BOOL)animated orderDelay:(int)orderDelay;
- (void)killWithAnimation:(BOOL)animated;

- (void)setFlagWithColor:(UIColor *)color;
- (void)setFlagWithImage:(UIImage *)image;
- (void)clearFlag;
- (BOOL)isFlag;

// Returns an autoreleased object. Retain the returned object to keep receiving updates, release it when you are done.
- (id)registerForPositionUpdatesWithCallback:(DCPushpinUpdateFn)callback;

+ (UIColor *)defaultHeadColor;

@end
