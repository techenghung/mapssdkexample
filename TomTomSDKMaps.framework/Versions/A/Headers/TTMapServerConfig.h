//
//  TTMapServerConfig.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 4/29/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum TTMapServerStyleType
{
	DCMAP_SERVER_STYLE_RASTER,
	DCMAP_SERVER_STYLE_VECTOR,
	DCMAP_SERVER_STYLE_BULK,
	DCMAP_SERVER_STYLE_BULK_RASTER,
	DCMAP_SERVER_STYLE_BULK_VECTOR,
	DCMAP_SERVER_STYLE_PREFETCH,
	DCMAP_SERVER_STYLE_PREFETCH_RASTER,
	DCMAP_SERVER_STYLE_PREFETCH_VECTOR,
	
	DCMAP_SERVER_STYLE_COUNT
}
TTMapServerStyleType;

@interface TTMapServerConfig : NSObject
{
	NSString	*server;
	NSString	*style;
	NSString	*user;
	NSString	*password;
    NSString    *auxilaryPassword;
	NSString	*onboardDirectory;
	NSString	*styleOverrides[DCMAP_SERVER_STYLE_COUNT];
	int			port;
}
@property (nonatomic, retain) NSString *server;
@property (nonatomic, assign) int port;
@property (nonatomic, retain) NSString *style;
@property (nonatomic, retain) NSString *user;
@property (nonatomic, retain) NSString *password;
@property (nonatomic, retain) NSString *auxilaryPassword;
@property (nonatomic, retain) NSString *onboardDirectory;

- (id)init;
- (id)initWithServer:(NSString *)server style:(NSString *)style user:(NSString *)user password:(NSString *)password;
- (id)initWithServer:(NSString *)server port:(int)port style:(NSString *)style user:(NSString *)user password:(NSString *)password;
- (void)setStyle:(TTMapServerStyleType)styleType toOverride:(NSString *)overrideType;
- (NSString *)getStyleOverride:(TTMapServerStyleType)styleType;
- (NSString **)getStyleOverridesArray;

@end
