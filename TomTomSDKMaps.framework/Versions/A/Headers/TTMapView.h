//
//  TTMapView.h
//  Sunder
//
//  Created by Daniel Posluns on 2/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "GEView.h"
#import "TTMapConstants.h"

#import <CoreLocation/CoreLocation.h>

@class CLHeading;
@class CLLocation;
@class TTFont;
@class TTFontFace;
@class TTFontOptions;
@class TTMapImpl;
@class TTMapLaunchState;
@class TTMapOrientation;
@class TTMapPopup;
@class TTPopupConfigCustomImage;
@class TTPopupConfigDrawOptions;
@class TTMapPos;
@protocol TTMapDelegate;
@protocol TTMapDataLayerDelegate;


@interface TTMapView : GEView
{
@private
    id<TTMapDelegate>	mapDelegate;
    TTMapLaunchState	*launchState;
    
    @package
    TTMapImpl			*_dcmapImpl;
}

@property (nonatomic, assign) id<TTMapDelegate> mapDelegate;
@property (nonatomic, assign) id<TTMapDataLayerDelegate> trafficIncidentsDelegate;
@property (nonatomic, assign) BOOL useBuiltInTrafficIncidentsPopups;
@property (nonatomic, retain) TTMapLaunchState *launchState;
@property (nonatomic, readonly) int zoomLevel;
@property (nonatomic, readonly) CLLocationCoordinate2D focusCoordinate;
@property (nonatomic, readonly) struct TTBoundingBox boundingBox;
@property (nonatomic, readonly) struct TTBoundingBox boundingBoxWithMargins;
@property (nonatomic, assign) BOOL nightMode;
@property (nonatomic, assign) BOOL compassVisible;
@property (nonatomic, assign) BOOL trafficVisible;
@property (nonatomic, assign) BOOL trafficIncidentsVisible;
@property (nonatomic, assign) BOOL satelliteVisible;
@property (nonatomic, assign) BOOL satelliteOnlyVisible;
@property (nonatomic, assign) BOOL alertVisible;
@property (nonatomic, assign) BOOL poiVisible;
@property (nonatomic, assign) CGRect uiMargins;
@property (nonatomic, assign) float overheadRatio;




- (id)initWithFrame:(CGRect)aRect;
- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)saveState;
- (void)setUIMargins:(CGRect)rect animationTime:(float)time;
- (void)setCompassPositionHorizontalRatio:(float)ratioX verticalRatio:(float)ratioY;

// Query the view for the zoom level that can fit a region on the map
- (int)zoomLevelToFitRegionWithCenter:(CLLocationCoordinate2D)center spanLatitude:(double)latitude spanLongitude:(double)longitude;
- (int)zoomLevelToFitRegionWithCenter:(CLLocationCoordinate2D)center spanLatitude:(double)latitude spanLongitude:(double)longitude horizontalMargin:(int)horizontalMargin verticalMargin:(int)verticalMargin;
- (int)zoomLevelToFitRegionWithNorthWest:(CLLocationCoordinate2D)northWest southEast:(CLLocationCoordinate2D)southEast;
- (int)zoomLevelToFitRegionWithNorthWest:(CLLocationCoordinate2D)northWest southEast:(CLLocationCoordinate2D)southEast horizontalMargin:(int)horizontalMargin verticalMargin:(int)verticalMargin;

// Manipulate the map
- (void)setMapOrientation:(TTMapOrientation *)orientation animated:(BOOL)animated;
- (void)zoomStep:(BOOL)zoomIn;
- (void)toggleOverhead;
- (void)setTrafficVisible:(BOOL)visible animated:(BOOL)animated;
- (void)setTrafficIncidentsVisible:(BOOL)visible animated:(BOOL)animated;
- (void)setSatelliteVisible:(BOOL)visible hybrid:(BOOL)hybrid animated:(BOOL)animated;
- (void)setAlertVisible:(BOOL)visible animated:(BOOL)animated;
- (void)setPOIVisible:(BOOL)visible animated:(BOOL)animated;
- (void)adjustScreenCenter:(CGPoint)screenCenterCoordinates;
- (void)resetScreenCenter;
- (void)setTileFormatConfiguration:(TileFormatConfiguration)configuration;

- (TTMapPos*)createMapPos:(CGPoint)uPix;

// Font management
- (TTFontFace *)loadFontFaceWithData:(NSData *)data;
- (TTFontFace *)loadFontFaceWithData:(NSData *)data customName:(NSString *)name;
- (TTFont *)fontWithName:(NSString *)name size:(int)points options:(TTFontOptions *)options;
- (TTFont *)fontWithFace:(TTFontFace *)face size:(int)points options:(TTFontOptions *)options;

// Popup management
- (void)configurePopupsWithDrawOptions:(TTPopupConfigDrawOptions *)options;
- (void)configurePopupsWithCustomImage:(TTPopupConfigCustomImage *)customImage;
- (void)presentPopup:(TTMapPopup *)popup;
- (void)closeCurrentPopup;

@end
