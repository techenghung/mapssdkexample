//
//  TTMapViewController.h
//  Sunder
//
//  Created by Daniel Posluns on 2/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "TTMapDelegate.h"

@class TTMapView;

@interface TTMapViewController : UIViewController <TTMapDelegate>
{
	// Primary
    TTMapView					*mapView;
	int							headerUIHeight;
}

@property (nonatomic, retain) IBOutlet TTMapView *mapView;

- (void)doneLoading:(TTMapView *)mapView;

@end
