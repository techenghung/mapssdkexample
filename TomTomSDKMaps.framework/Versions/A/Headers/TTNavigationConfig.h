//
//  TTNavigationConfig.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 6/26/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TomTomSDKMaps.h"


@class TTConfigFile;

/** 
 An abstraction of a set of configurations that deal with the navigation server, logging, and directories paths.
 
 @discussion 
 Directories may be left nil to use their defaults:<br>
	- configDir: Documents directory
	- resourceDir: Main bundle resource path
	- dataDir: Documents directory
	- applicationDir: Documents directory
	- logDir: Documents directory

  The server must always be specified.<br>
  The port may be directly specified or left at 0 (i.e. unspecified, which will resolve to 80).
*/
@interface TTNavigationConfig : NSObject
{
	NSString	*server;
	NSString	*configDir;
	NSString	*resourceDir;
	NSString	*dataDir;
	NSString	*applicationDir;
	NSString	*logDir;
	NSString	*configProperties;
	NSString	*loggingProperties;
	NSString	*serverKey;
	NSString	*NKWServerKey;
    NSString	*speechKey;
    NSString	*currentLocale;
    int			serverPort;
	TTLogLevel	fileLogLevel;
	TTLogLevel	consoleLogLevel;
    TTConfigFile *configFile;
}
@property (nonatomic, retain) NSString *server;/**Navigation server address (Chameleon) <b>mandatory</b>*/
@property (nonatomic, retain) NSString *configDir;/**Defaults to Documents Directory*/
@property (nonatomic, retain) NSString *resourceDir;/**Defaults to the Main bundle resource path*/
@property (nonatomic, retain) NSString *dataDir;/**Defaults to Documents Directory*/
@property (nonatomic, retain) NSString *applicationDir;/**Defaults to Documents Directory*/
@property (nonatomic, retain) NSString *logDir;/**Defaults to Cache Directory/navlib/runlogs*/
@property (nonatomic, retain) NSString *configProperties;/**String contents of config.properties, defaults to nil*/
@property (nonatomic, retain) NSString *loggingProperties;/**String contents of logging.properties, defaults to nil*/
@property (nonatomic, retain) NSString *serverKey;/**Chameleon Server API Key*/
@property (nonatomic, retain) NSString *NKWServerKey;/**NKW Server API Key*/
@property (nonatomic, retain) NSString *speechKey;/**Online speech API Key*/
@property (nonatomic, retain) NSString *currentLocale;/**Locale to start API with*/
@property (nonatomic, assign) int serverPort;/**Navigation server port number, defaults to 0 which will resolve to 80*/
@property (nonatomic, assign) TTLogLevel fileLogLevel;/**Sets the log level for the log file, for levels @see TTLogLevel */
@property (nonatomic, assign) TTLogLevel consoleLogLevel;/**Sets the log level for the console, for levels @see TTLogLevel */

- (id)initWithServer:(NSString *)aServer;
- (id)initWithServer:(NSString *)aServer port:(int)aPort;
- (id)initWithServer:(NSString *)aServer port:(int)aPort key:(NSString *)aKey;
- (id)initWithConfigFile:(TTConfigFile *)config;
- (void)populateDefaults;
- (void)shouldUseNKW:(BOOL)useNKW;

+ (TTNavigationConfig *)configWithServer:(NSString *)server;
+ (TTNavigationConfig *)configWithServer:(NSString *)server port:(int)port;
+ (TTNavigationConfig *)configWithConfigFile:(TTConfigFile *)config;
+ (NSString *)defaultConfigDir;
+ (NSString *)defaultResourceDir;
+ (NSString *)defaultDataDir;
+ (NSString *)defaultApplicationDir;
+ (NSString *)defaultLogDir;
+ (TTLogLevel)defaultFileLogLevel;
+ (TTLogLevel)defaultConsoleLogLevel;

@end
