//
//  TTNavigationDelegate.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 6/27/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TTGuidanceConstants.h"

@class TTNavigationManager;
@class TTNavigationUpdate;

/**
 The main protocol for receiving navigation events.
 */
@protocol TTNavigationDelegate <NSObject>

@optional
/**
 Called at every navigation update.
 */
- (void)navigationManager:(TTNavigationManager *)manager
                   update:(TTNavigationUpdate *)update;
/**
 Called when starting a negotiation session with the guidance server for credentials. 
 Ideally setup should occur at least once since it is persistent.
 */
- (void)navigationManagerSetupStarted:(TTNavigationManager *)manager;
/**
 Called at the successful conclusion of setup.
*/
- (void)navigationManagerSetupFinished:(TTNavigationManager *)manager;
/**
 Called if there were a failure in the setup process.
*/
- (void)navigationManager:(TTNavigationManager *)manager
     setupFailedWithError:(TTGuidanceSetupError)error;
/**
 Called when route is requested from the server.
*/
- (void)navigationManagerRouteRequested:(TTNavigationManager *)manager;
/**
 Called when the requested route is successfully downloaded and is now available.
*/
- (void)navigationManagerRouteAvailable:(TTNavigationManager *)manager;
/**
 Called if an error occurs in the route request process.
*/
- (void)navigationManager:(TTNavigationManager *)manager
     routeFailedWithError:(TTGuidanceRouteError)error;
/**
 Called when the destination of the route is reached.
*/
- (void)navigationManagerDestinationReached:(TTNavigationManager *)manager;
/**
 Called at every reroute. If there were an excessive rerouting 
    issue a recommendation will be sent to ask the user if they want to abort.
 */
- (void)navigationManager:(TTNavigationManager *)manager
rerouteWithRecommendedAbort:(BOOL)recommendAbort;
/**
 Called when alternative route is available.
 */
- (void)navigationManager:(TTNavigationManager *)manager
 alternativeRouteAvailableWithCurrentRouteTravelTime:(long)currentRouteTravelTime
               withCurrentRouteDistanceToDestination:(long)currentRouteDistanceToDestination
                      withAlternativeRouteTravelTime:(long)alternativeRouteTravelTime
           withAlternativeRouteDistanceToDestination:(long)alternativeRouteDistanceToDestination;
/**
 Called when the navigation manager is waiting for reliable sensor information.
 A route will not be requested until valid location data exists. 
 Unless the route is cancelled the library will continue to wait until 
    valid location data has been received.
 */
- (void)navigationManagerWaitingForGPS:(TTNavigationManager *)manager;
/**
 Called if the manager has been unable to receive location data for a while.
 It immediately precedes the voice announcement “GPS lost”.
*/
- (void)navigationManagerGPSUnavailable:(TTNavigationManager *)manager;
/**
 Called if the user has disallowed the use of location data.
 The routing request has been cancelled, and will only be re-issued
    if the application invokes startGuidance again.
 Does not apply to simulation or sensorlog playback.
 */
- (void)navigationManagerGPSDenied:(TTNavigationManager *)manager;
/**
 Called if the navigation manager is about the make a voice guidance announcement.
 Allows applications using the navigation framework to turn down or mute 
    their own audio before the guidance is announced.
 */
- (void)navigationManagerAudioStarted:(TTNavigationManager *)manager;
/**
 Called if the navigation manager is finished making a guidance announcement.
 An application may use this and the preceding AudioStarted to control its own use 
    of audio so that guidance announcements are not played on top of other 
    on-going audio from the application.
 */
- (void)navigationManagerAudioFinished:(TTNavigationManager *)manager;


/**
  Called if the navigation manager receives a route with a snapped origin point, 
    meaning that the requested route failed to be planned using the requested origin
    and a new origin was provided within a radius of the failing origin.
 */
- (void)navigationManagerOriginHasChanged:(TTNavigationManager *)manager;

@end
