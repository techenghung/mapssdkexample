//
//  TTNavigationManager.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 5/10/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLLocation;
@class TTGuidanceConfig;
@class TTGuidanceUpdate;
@class TTNavigationConfig;
@class TTNavigationImpl;
@class TTNavigationUpdate;
@protocol TTNavigationDelegate;
@protocol TTGuidanceUpdate;

typedef void (^TTNavUpdateFn)(TTNavigationUpdate *);

/**
 Main interface to Navigation services, it provides means to start/stop guidance, and initiates callbacks for various navigation updates.
*/
@interface TTNavigationManager : NSObject
{
@private

@package
	TTNavigationImpl	*_dcnavImpl;
}

/**
 navigation configuration property.
 Configurations for:<br>
    - Navigation server/port<br>
    - Log path and level<br>
    - Directories for data, application, resources and config all with default values.
 */
@property (nonatomic, readonly) TTNavigationConfig *navigationConfig;

/**
 Sets navigation related configurations (destination, origin, etc).
*/
@property (nonatomic, readonly) TTGuidanceConfig *guidanceConfig;

/**
 Contains information about the last guidance update we have (street name, route points, etc).
*/
@property (nonatomic, readonly) id<TTGuidanceUpdate> lastGuidance;

/**
 Contains information about the last navigation update we have
 (last announcement, vehicle position and direction, if we reached our destination or not, etc).
*/
@property (nonatomic, readonly) TTNavigationUpdate *lastUpdate;

/**
 Alternative route search interval in seconds.
 */
@property (nonatomic, assign) long alternativeRouteSearchInterval;

/**
 Alternative route to current route travel time ratio. This ratio is used as
 acceptance criteria before alternative route is reported to the client.
 */
@property (nonatomic, assign) float alternativeRouteTravelTimeRatio;

/**
 If the audio is muted or not.
*/
@property (nonatomic, assign) BOOL audioMuted;

/**
 Manager initializer with a config file.
 @discussion This the class's custom initializer and it accepts a configuration object that's going to guide the creation of the construction.
 @param config The configuration object to construct a TTNavigationManager with
 @return id The newely constructed object
 */
- (id)initWithConfig:(TTNavigationConfig *)config;

/**
 Returns an autoreleased object. Retain the returned object to keep receiving updates, <b>release it when you are done.</b>
 @param delegate the delegate object that's going to receive all the consequent events for all the protocol methods that it implements.
 @return id an instance of NavCallback that contains a reference to the delegate, can be safely ignored as events will be delivered to the delegate without any further action
 */
- (id)registerForNavigationUpdatesWithDelegate:(id<TTNavigationDelegate>)delegate;

/**
 Start of the Guidance service – This is the entry point for guidance.
 @param config configuration object that contains route-related configurations.
 */
- (void)startGuidance:(TTGuidanceConfig *)config;

/**
 Stops the Guidance service and starts the Tracking service, in order to totally shutdown the navigation service the TTNavigationManager reference has to be released.
 */
- (void)stopGuidance;

/**
 Repeats the last maneuver announcement audio we had (turn left, turn right, etc.)
 */
- (void)repeatManeuverAudio;

/**
 Accepts alternative route
 */
- (void)acceptAlternativeRoute;

@end
