//
//  TTNavigationUpdate.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 5/16/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

#import "TTGuidanceConstants.h"

@protocol TTGuidanceAnnouncement;
@protocol TTGuidanceUpdate;

@interface TTNavigationUpdate : NSObject
{
	id<TTGuidanceUpdate>		guidance;
	id<TTGuidanceAnnouncement>	announcement;
	CLLocationCoordinate2D		vehiclePosition;
	CLLocationDegrees			vehicleDirection;
	float						speedKPH;
	float						accuracyMeters;
	TTGuidancePositionSource	positionSource;
	BOOL						destinationReached;
    NSString *                  routeID;
}

@property (nonatomic, retain) id<TTGuidanceUpdate> guidance;
@property (nonatomic, retain) id<TTGuidanceAnnouncement> announcement;
@property (nonatomic, assign) CLLocationCoordinate2D vehiclePosition;
@property (nonatomic, assign) CLLocationDegrees vehicleDirection;
@property (nonatomic, assign) float speedKPH;
@property (nonatomic, assign) float accuracyMeters;
@property (nonatomic, assign) TTGuidancePositionSource positionSource;
@property (nonatomic, assign) BOOL destinationReached;
@property (nonatomic, assign) NSString *routeID;

@end
