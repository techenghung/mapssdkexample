//
//  TTOverlayCircle.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 8/7/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@class TTMapView;
@class DCCircleImpl;

@interface TTOverlayCircle : NSObject
{
	UIColor			*color;
	
@package
	DCCircleImpl	*_circleImpl;
}
@property (nonatomic, assign) BOOL visible;
@property (nonatomic, retain) UIColor *color;

- (id)initWithMap:(TTMapView *)map coordinate:(CLLocationCoordinate2D)coordinate longitudeRadius:(CLLocationDegrees)radius;


@end
