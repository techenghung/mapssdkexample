//
//  TTOverlayPolygon.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 8/6/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@class TTMapView;
@class DCPolygonImpl;

@interface TTOverlayPolygon : NSObject
{
	UIColor			*color;
	
@package
	DCPolygonImpl	*_polygonImpl;
}
@property (nonatomic, assign) BOOL visible;
@property (nonatomic, retain) UIColor *color;

- (id)initWithMap:(TTMapView *)map coordinates:(CLLocationCoordinate2D *)coordinates count:(size_t)count;

@end
