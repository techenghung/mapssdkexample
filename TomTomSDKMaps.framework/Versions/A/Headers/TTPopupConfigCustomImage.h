//
//  TTPopupConfigCustomImage.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 1/31/14.
//  Copyright (c) 2014 Tomtom. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TTBitmap;

@interface TTPopupConfigCustomImage : NSObject
{
	TTBitmap	*bitmap;
	CGPoint		outerTopLeftCoord;
	CGPoint		innerTopLeftCoord;
	CGPoint		innerBottomRightCoord;
	CGPoint		outerBottomRightCoord;
	CGRect		innerMargins;
	int			tailHorizontalInset;
	int			tailWidth;
	int			tailDescent;
	CGPoint		tailHotspot;
	CGPoint		backgroundOffset;
	UIColor		*backgroundTopColor;
	UIColor		*backgroundBottomColor;
}
@property (nonatomic, retain) TTBitmap *bitmap;
@property (nonatomic, assign) CGPoint outerTopLeftCoord;
@property (nonatomic, assign) CGPoint innerTopLeftCoord;
@property (nonatomic, assign) CGPoint innerBottomRightCoord;
@property (nonatomic, assign) CGPoint outerBottomRightCoord;
@property (nonatomic, assign) CGRect innerMargins;
@property (nonatomic, assign) int tailHorizontalInset;
@property (nonatomic, assign) int tailWidth;
@property (nonatomic, assign) int tailDescent;
@property (nonatomic, assign) CGPoint tailHotspot;
@property (nonatomic, assign) CGPoint backgroundOffset;
@property (nonatomic, retain) UIColor *backgroundTopColor;
@property (nonatomic, retain) UIColor *backgroundBottomColor;

- (id)initWithBitmap:(TTBitmap *)bitmap;


@end
