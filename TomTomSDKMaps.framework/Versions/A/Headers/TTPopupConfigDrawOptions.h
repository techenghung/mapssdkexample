//
//  TTPopupConfigDrawOptions.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 1/31/14.
//  Copyright (c) 2014 Tomtom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTPopupConfigDrawOptions : NSObject
{
	UIColor		*backgroundTopColor;
	UIColor		*backgroundBottomColor;
	UIColor		*outlineColor;
	UIColor		*shadowColor;
	CGPoint		shadowOffset;
	int			shadowBlurRadius;
	int			outlineThickness;
	float		cornerRadius;
	int			tailWidth;
	int			tailHeight;
}
@property (nonatomic, retain) UIColor *backgroundTopColor;
@property (nonatomic, retain) UIColor *backgroundBottomColor;
@property (nonatomic, retain) UIColor *outlineColor;
@property (nonatomic, retain) UIColor *shadowColor;
@property (nonatomic, assign) CGPoint shadowOffset;
@property (nonatomic, assign) int shadowBlurRadius;
@property (nonatomic, assign) int outlineThickness;
@property (nonatomic, assign) float cornerRadius;
@property (nonatomic, assign) int tailWidth;
@property (nonatomic, assign) int tailHeight;

- (id)init;

+ (TTPopupConfigDrawOptions *)drawOptions;

+ (UIColor *)defaultBackgroundTopColor;
+ (UIColor *)defaultBackgroundBottomColor;
+ (UIColor *)defaultOutlineColor;
+ (UIColor *)defaultShadowColor;
+ (CGPoint)defaultShadowOffset;
+ (int)defaultShadowBlurRadius;
+ (int)defaultOutlineThickness;
+ (float)defaultCornerRadius;
+ (int)defaultTailWidth;
+ (int)defaultTailHeight;

@end
