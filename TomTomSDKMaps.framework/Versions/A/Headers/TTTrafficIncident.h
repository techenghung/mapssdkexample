//
//  TTTrafficIncident.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 6/24/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>
#import <CoreGraphics/CoreGraphics.h>

typedef enum TTTrafficIncidentCategory
{
    TTTrafficIncidentCategoryUnknown,
    TTTrafficIncidentCategoryAccident,
    TTTrafficIncidentCategoryFog,
    TTTrafficIncidentCategoryDangerousConditions,
    TTTrafficIncidentCategoryRain,
    TTTrafficIncidentCategoryIce,
    TTTrafficIncidentCategoryJam,
    TTTrafficIncidentCategoryLaneClosed,
    TTTrafficIncidentCategoryRoadClosed,
    TTTrafficIncidentCategoryRoadWorks,
    TTTrafficIncidentCategoryWind,
    TTTrafficIncidentCategoryFlooding,
    TTTrafficIncidentCategoryDetour,
    TTTrafficIncidentCategoryCluster
}
TTTrafficIncidentCategory;

@interface TTTrafficIncident : NSObject

@property (nonatomic, readonly, copy) NSString *identifier;
@property (nonatomic, readonly) TTTrafficIncidentCategory category;
@property (nonatomic, readonly) CLLocationCoordinate2D position;
@property (nonatomic, readonly) CLLocationCoordinate2D originalPosition;
@property (nonatomic, readonly) int magnitude;
@property (nonatomic, readonly) CGRect clusterBBox;
@property (nonatomic, readonly, copy) NSString *description;
@property (nonatomic, readonly, copy) NSString *cause;
@property (nonatomic, readonly, copy) NSString *from;
@property (nonatomic, readonly, copy) NSString *to;
@property (nonatomic, readonly) int length;
@property (nonatomic, readonly) int delay;
@property (nonatomic, readonly, copy) NSArray<NSString*> *roadNumbers;
@property (nonatomic, readonly, copy) NSArray<TTTrafficIncident*> *clusterContent;

- (id)initWithIdentifier:(NSString*)identifier
            withCategory:(TTTrafficIncidentCategory)category
            withPosition:(CLLocationCoordinate2D)position
    withOriginalPosition:(CLLocationCoordinate2D)originalPosition
           withMagnitude:(int)magnitude
         withClusterBBox:(CGRect)clusterBBox
         withDescription:(NSString*)description
               withCause:(NSString*)cause
                withFrom:(NSString*)from
                  withTo:(NSString*)to
              withLength:(int)length
               withDelay:(int)delay
         withRoadNumbers:(NSArray<NSString*>*)roadNumbers
      withClusterContent:(NSArray<TTTrafficIncident*>*)clusterContent;
@end

