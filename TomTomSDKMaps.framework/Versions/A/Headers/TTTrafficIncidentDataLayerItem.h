//
//  TTTrafficIncidentDataLayerItem.h
//  TomTomSDKMaps
//
//  Created by Kapica on 11/03/16.
//  Copyright © 2016 Tomtom. All rights reserved.
//

#import "TTMapDataLayerItem.h"

@class TTTrafficIncident;

@interface TTTrafficIncidentDataLayerItem : TTMapDataLayerItem
@property (nonatomic, readonly) TTTrafficIncident *trafficIncident;
@end
