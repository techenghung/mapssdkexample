//
//  TomTomSDKMaps.h
//  TomTomSDKMaps
//
//  Created by Daniel Posluns on 4/25/13.
//  Copyright (c) 2013 Tomtom. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TTBitmap.h"
#import "TTBitmapRegion.h"
#import "TTConfigFile.h"
#import "TTConstants.h"
#import "TTFont.h"
#import "TTFontFace.h"
#import "TTFontOptions.h"
#import "TTFormattedText.h"
#import "TTGuidanceAnnouncement.h"
#import "TTGuidanceConfig.h"
#import "TTGuidanceConstants.h"
#import "TTGuidanceIcon.h"
#import "TTGuidanceListItem.h"
#import "TTGuidanceSimpleIcon.h"
#import "TTGuidanceUpdate.h"
#import "TTGuidanceLane.h"
#import "TTMapConstants.h"
#import "TTMapDelegate.h"
#import "TTMapIcon.h"
#import "TTMapIconDelegate.h"
#import "TTMapLaunchState.h"
#import "TTMapOrientation.h"
#import "TTMapOrientation+TTMapView.h"
#import "TTMapPopup.h"
#import "TTMapPopupLayout.h"
#import "TTMapPushpin.h"
#import "TTMapPushpinDelegate.h"
#import "TTMapView.h"
#import "TTMapViewController.h"
#import "TTNavigationConfig.h"
#import "TTNavigationDelegate.h"
#import "TTNavigationManager.h"
#import "TTNavigationUpdate.h"
#import "TTNavLaunchState.h"
#import "TTNavViewController.h"
#import "TTOverlayCircle.h"
#import "TTOverlayPolygon.h"
#import "TTOverlayPolyline.h"
#import "TTPopupConfigCustomImage.h"
#import "TTPopupConfigDrawOptions.h"
#import "GEView.h"
#import "TTMapDataLayer.h"
#import "TTMapDataLayerItem.h"
#import "TTTrafficIncident.h"
#import "TTTrafficIncidentDataLayerItem.h"
#import "TTMapPos.h"

@interface TomTomSDKMaps : NSObject

+ (int)getBuildNumber;
+ (NSString *)getLibraryHash;
+ (NSString *)getLibraryVersion;

@end
