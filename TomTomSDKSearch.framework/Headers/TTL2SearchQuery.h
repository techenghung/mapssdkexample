/**
 * Copyright (c) 2017 TomTom N.V. All rights reserved.
 *
 * This software is the proprietary copyright of TomTom N.V. and its subsidiaries and may be used
 * for internal evaluation purposes or commercial use strictly subject to separate licensee
 * agreement between you and TomTom. If you are the licensee, you are only permitted to use
 * this Software in accordance with the terms of your license agreement. If you are not the
 * licensee then you are not authorised to use this software in any manner and should
 * immediately return it to TomTom N.V.
 */

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "TTSupportedLanguages.h"

@class TTL2SearchQuery;

@interface TTL2SearchQueryBuilder : NSObject
+(TTL2SearchQueryBuilder*)createWithTerm:(NSString*)term;
-(TTL2SearchQueryBuilder*)withLimit:(NSNumber *)limit;
-(TTL2SearchQueryBuilder*)withOffset:(NSNumber *)offset;
-(TTL2SearchQueryBuilder*)withPostion:(CLLocationCoordinate2D)position;
-(TTL2SearchQueryBuilder*)withPostion:(CLLocationCoordinate2D)position withRadius:(int)radius;
-(TTL2SearchQueryBuilder*)withCountrySet:(NSArray<NSString*>*)countrySet;
-(TTL2SearchQueryBuilder*)withIdxSet:(NSArray<NSNumber*> *)idxSet;
-(TTL2SearchQueryBuilder*)withLanguage:(TTLanguage)languageType;
-(TTL2SearchQueryBuilder*)withBoundingBoxTopLeft:(CLLocationCoordinate2D)topLeft
                      withBoundingBoxBottomRight:(CLLocationCoordinate2D)bottomRight;
-(TTL2SearchQueryBuilder*)withTypeAhead:(BOOL)isTypeAhead;
-(TTL2SearchQueryBuilder*)withCategory:(BOOL)isCategory;
-(TTL2SearchQueryBuilder*)withMinFuzzyLevel:(NSNumber *)minFuzzyLevel;
-(TTL2SearchQueryBuilder*)withMaxFuzzyLevel:(NSNumber *)maxFuzzyLevel;
-(TTL2SearchQuery*)build;
@end

typedef NS_ENUM(NSUInteger, CoordinateValue) {
    CoordinateValuePosition,
    CoordinateValueBoundingBoxTopLeft,
    CoordinateValueBoundingBoxBottomRight
};

typedef NS_ENUM(NSUInteger, IdxSet) {
    IdxSetAddressRangeInterpolation,
    IdxSetGeographies,
    IdxSetPointAddresses,
    IdxSetPointsOfInterest,
    IdxSetStreets,
    IdxSetCrossStreets,
};

@interface TTL2SearchQuery : NSObject
@property (nonatomic, readonly, copy) NSString *term;
@property (nonatomic, readonly, copy) NSValue *position;
@property (nonatomic, readonly, copy) NSNumber *radius;
@property (nonatomic, readonly, copy) NSNumber *limit;
@property (nonatomic, readonly, copy) NSNumber *offset;
@property (nonatomic, readonly, copy) NSArray<NSString*> *countrySet;
@property (nonatomic, readonly, copy) NSArray<NSNumber*> *idxSet;
@property (nonatomic, readonly, copy) NSValue *boundingBoxTopLeft;
@property (nonatomic, readonly, copy) NSValue *boundingBoxBottomRight;
@property (nonatomic, readonly, assign) BOOL isTypeAhead;
@property (nonatomic, readonly, assign) BOOL isCategory;
@property (nonatomic, readonly, assign) TTLanguage language;
@property (nonatomic, readonly, copy) NSNumber *minFuzzyLevel;
@property (nonatomic, readonly, copy) NSNumber *maxFuzzyLevel;

+ (NSDictionary *)idxSetDict;

- (CLLocationCoordinate2D)coordinateForNSValue:(CoordinateValue)coordinateValue;

@end

