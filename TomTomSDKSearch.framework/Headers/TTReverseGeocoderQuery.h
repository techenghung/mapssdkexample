/**
 * Copyright (c) 2017 TomTom N.V. All rights reserved.
 *
 * This software is the proprietary copyright of TomTom N.V. and its subsidiaries and may be used
 * for internal evaluation purposes or commercial use strictly subject to separate licensee
 * agreement between you and TomTom. If you are the licensee, you are only permitted to use
 * this Software in accordance with the terms of your license agreement. If you are not the
 * licensee then you are not authorised to use this software in any manner and should
 * immediately return it to TomTom N.V.
 */

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "TTSupportedLanguages.h"

extern NSString *const TTReverseGeocoderAttributeCountry;
extern NSString *const TTReverseGeocoderAttributeCountryCode;
extern NSString *const TTReverseGeocoderAttributeCountryCodeISO3;
extern NSString *const TTReverseGeocoderAttributeCountrySubdivision;
extern NSString *const TTReverseGeocoderAttributeCountrySubdivisionName;
extern NSString *const TTReverseGeocoderAttributeCountrySecondarySubdivision;
extern NSString *const TTReverseGeocoderAttributeMunicipality;
extern NSString *const TTReverseGeocoderAttributeMunicipalitySubdivision;
extern NSString *const TTReverseGeocoderAttributeStreet;
extern NSString *const TTReverseGeocoderAttributeStreetName;
extern NSString *const TTReverseGeocoderAttributeStreetNameAndNumber;
extern NSString *const TTReverseGeocoderAttributePostalCode;
extern NSString *const TTReverseGeocoderAttributeBuildingNumber;
extern NSString *const TTReverseGeocoderAttributeStreetNumber;
extern NSString *const TTReverseGeocoderAttributeFreeformAddress;

@interface TTReverseGeocoderQuery : NSObject

@property (nonatomic, readonly, assign) NSValue *coordinate;
@property (nonatomic, readonly, assign) CLLocationCoordinate2D locationCoordinate;
@property (nonatomic, readonly, copy) NSNumber *radius;
@property (nonatomic, readonly, assign) TTLanguage language;
@property (nonatomic, readonly, assign) NSString *languagePrefix;
@property (nonatomic, readonly, assign) NSNumber *heading;
@property (nonatomic, readonly, copy) NSString *number;
@property (nonatomic, readonly, assign) BOOL returnSpeedLimit;
@property (nonatomic, readonly, assign) BOOL returnRoadUse;
@property (nonatomic, readonly, assign) NSNumber *tolerance;
@property (nonatomic, readonly, assign) BOOL returnMatchType;
@property (nonatomic, readonly, copy) NSArray<NSString *> *addressAttributes;

@end

@interface TTReverseGeocoderQueryBuilder : NSObject

+(TTReverseGeocoderQueryBuilder*)createWithCLLocationCoordinate2D:(CLLocationCoordinate2D)coordinate;
-(TTReverseGeocoderQueryBuilder*)withRadius:(NSNumber *)radius;
-(TTReverseGeocoderQueryBuilder*)withLanguage:(TTLanguage)languageType;
-(TTReverseGeocoderQueryBuilder*)withHeading:(NSNumber *)heading;
-(TTReverseGeocoderQueryBuilder*)withNumber:(NSString *)number;
-(TTReverseGeocoderQueryBuilder*)withReturnSpeedLimit:(BOOL)returnSpeedLimit;
-(TTReverseGeocoderQueryBuilder*)withReturnRoadUse:(BOOL)returnRoadUse;
-(TTReverseGeocoderQueryBuilder*)withTolerance:(NSNumber *)tolerance;
-(TTReverseGeocoderQueryBuilder*)withReturnMatchType:(BOOL)returnMatchType;
-(TTReverseGeocoderQueryBuilder*)withAddressAttributes:(NSArray<NSString *> *)addressAttributes;
-(TTReverseGeocoderQuery*)build;

@end

