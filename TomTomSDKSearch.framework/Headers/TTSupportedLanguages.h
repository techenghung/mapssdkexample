/**
 * Copyright (c) 2017 TomTom N.V. All rights reserved.
 *
 * This software is the proprietary copyright of TomTom N.V. and its subsidiaries and may be used
 * for internal evaluation purposes or commercial use strictly subject to separate licensee
 * agreement between you and TomTom. If you are the licensee, you are only permitted to use
 * this Software in accordance with the terms of your license agreement. If you are not the
 * licensee then you are not authorised to use this software in any manner and should
 * immediately return it to TomTom N.V.
 */

#ifndef TTSupportedLanguages_h
#define TTSupportedLanguages_h

typedef NS_ENUM(NSUInteger, TTLanguage) {
    EnglishUS = 0,
    Afrikaans,
    Arabic,
    Bulgarian,
    Catalan,
    ChineseCN,
    ChineseTW,
    Czech,
    Danish,
    DutchBE,
    DutchNL,
    EnglishAU,
    EnglishNZ,
    EnglishGB,
    Estonian,
    Finnish,
    FrenchCA,
    FrenchFR,
    German,
    Greek,
    Croatian,
    Hungarian,
    Italian,
    Latvian,
    Lithuanian,
    Malay,
    Norwegian,
    Polish,
    PortugueseBR,
    PortuguesePT,
    Russian,
    Slovak,
    Slovenian,
    SpanishES,
    SpanishLatin,
    Swedish,
    Turkish
};


#endif /* TTSupportedLanguages_h */
