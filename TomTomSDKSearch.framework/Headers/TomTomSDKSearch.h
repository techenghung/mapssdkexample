/**
 * Copyright (c) 2017 TomTom N.V. All rights reserved.
 *
 * This software is the proprietary copyright of TomTom N.V. and its subsidiaries and may be used
 * for internal evaluation purposes or commercial use strictly subject to separate licensee
 * agreement between you and TomTom. If you are the licensee, you are only permitted to use
 * this Software in accordance with the terms of your license agreement. If you are not the
 * licensee then you are not authorised to use this software in any manner and should
 * immediately return it to TomTom N.V.
 */

#import <UIKit/UIKit.h>

//! Project version number for TomTomSDKSearch.
FOUNDATION_EXPORT double TomTomSDKSearchVersionNumber;

//! Project version string for TomTomSDKSearch.
FOUNDATION_EXPORT const unsigned char TomTomSDKSearchVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TomTomSDKSearch/TomTomSDKSearch.h>


#import "TTSupportedLanguages.h"
#import "TTResponseError.h"
#import "TTL2Search.h"
#import "TTL2SearchQuery.h"
#import "TTL2SearchResult.h"
#import "TTReverseGeocoder.h"
#import "TTReverseGeocoderQuery.h"
#import "TTReverseGeocoderResult.h"
#import "TTReverseGeocoderSummary.h"
#import "TTReverseGeocoderFullAddress.h"
#import "TTReverseGeocoderAddress.h"
