# Data source for map data is based on:

General Copyrights: "© 1992 - 2016 TomTom. All rights reserved. This material is proprietary and the subject of copyright protection, database right protection and other intellectual property rights owned by TomTom or its suppliers. The use of this material is subject to the terms of a license agreement. Any unauthorized copying or disclosure of this material will lead to criminal and civil liabilities."

For Austria: “© BEV, GZ 1368/2013.”

For Denmark: “© DAV, violation of these copyrights shall cause legal proceedings.”

For France: “Michelin data © Michelin 2013.”

For Germany: “GeoBasis-DE/Geobasis NRW 2013.”

For Indonesia: “© Base data Bakosurtanal”.

For Jordan: “© Royal Jordanian Geographic center”

For Malta: “This product includes data from Mapping Unit, Malta Environment and Planning Authority and licensed on behalf of them” or “© Mapping Unit, Malta Environment and Planning Authority.”

For Northern Ireland: “Ordnance Survey of Northern Ireland.”

For Norway: “© Norwegian Mapping Authority, Public Roads Administration / © Mapsolutions.”

For Russia: “© ROSREESTR”

For Switzerland: “© Swisstopo.”

For The Netherlands: “Topografische onderground Copyright © dienst voor het kadaster en de openbare registers, Apeldoorn 2013.”

For United Kingdom (excluding Northern Ireland):
“Contains Ordnance Survey data © Crown copyright and database right 2013.”

Code-Point® Open data:
“Contains Royal Mail data © Royal Mail copyright and database right 2013.”

Data source for MultiNet North America: The product includes information copied with permission from Canadian authorities, including © Canada Post Corporation, GeoBase®, and Department of Natural Resources Canada, All rights reserved. The use of this material is subject to the terms of a License Agreement. You will be held liable for any unauthorized copying or disclosure of this material. Adapted from Statistics Canada: Road Network File, 2011; and Census Population and Dwelling Count Highlight Tables, 2011 Census. This does not constitute an endorsement by Statistics Canada of this product.”

Portions of the POI database North America have been provided by Localeze. The use of this material is subject to the terms of a license agreement. Any unauthorized copying or disclosure of this material will lead to criminal and civil liabilities.”

Data source for Japan map data: “©Shobunsha Publications, Inc. ©Shobunsha”

Data source for New Zealand: “GeoSmart ® map data is © 2013 GeoSmart Maps Limited and its licensors, GeoSmart® is a registered trademark of GeoSmart Maps Limited of New Zealand”

Data source for Australia: “® Registered trademark of Telstra Corporation Limited (ABN 33 051 775 556)”

Data Source for TomTom North America HD Traffic

Portions of the data have been provided by Clear Channel Broadcasting © 2013. Clear Channel Broadcasting, Inc. All rights reserved.
