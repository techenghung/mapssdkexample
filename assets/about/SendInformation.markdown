This app only works if it can send information to TomTom. this information includes your location. We need your permission to enable this app and use your information.

We at TomTom also use the information that we receive to improve our products and services, but only after we have made sure that it is not linked to you in any way.

Will you allow your information to be sent when required?