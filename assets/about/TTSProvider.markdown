# TTS provider
This application uses Nuance as a provider for text to speech.

Copyright © 2002-2016 Nuance Communications, Inc.
All rights reserved.

![logo](nuance-logo.png)
