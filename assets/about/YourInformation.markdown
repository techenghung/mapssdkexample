TomTom software uses your location

To work correctly, TomTom software uses location information from your device.

Some features of TomTom software have to send this location information to TomTom. The software also sends information that identifies your device, tells us how you use the software and includes information you enter.

We need your permission before we can use your information and enable these features.

We use the information so we can do the following:

Send information to your device that is relevant to where you are.
Improve the quality of our products and services, but only after we have made sure the information cannot be linked back to you.
The information is sent using the internet. If an internet connection is not available, the information is stored securely on your device to be uploaded later.

Service management and improvement

TomTom keeps information that tells us how and when you use our services. This includes information about the device you are using and the information we receive while you use the service, such as locations, routes, destinations and search queries. TomTom is unable to identify you based on the information it collects, and will not try to. TomTom uses the information for technical diagnostics, to detect fraud and abuse, to create usage reports, and to improve its services. The information is kept only for these purposes and for a limited period of time, after which it is destroyed. TomTom applies security methods based on industry standards to protect the information against unauthorised access.

You should be aware that while running the TomTom software on your mobile phone, your device may run operating system software or other apps, or use communication networks, which collect, transmit or store personal and location information. TomTom has no influence over this. For more details refer to the information and settings provided by your mobile operator and the vendors of your device and apps.

Thank you and our promise

In choosing to provide TomTom with information you are helping to make driving better, specifically by improving maps, traffic flows and reducing congestion. Thank you. We promise not give anyone else access to the information that we collect or use it for any other purpose, unless explicitly and lawfully ordered to do so following due legal process.

Contacting us

If you think that your information is not being used for the purpose for which you have provided it to TomTom, contact us at http://tomtom.com/support.

See our privacy policy at http://tomtom.com/support.